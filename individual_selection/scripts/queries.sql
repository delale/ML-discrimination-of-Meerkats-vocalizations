# timediff of females
SELECT IndividID, Code, Sex, FirstGroup, LastGroup, BirthDate, FirstSighting, LastSighting, DATEDIFF(LastSighting, FirstSighting)/365 AS TimeDiff, Fate
FROM MeerkatViews.qry_LifeHistoryBasic
WHERE Sex = 'F'
ORDER BY TimeDiff DESC;


# file num for F
SELECT B.IndividID, count(*) AS NumFiles 
FROM KMP_SoundDatabase.tblSoundFiles B
JOIN (SELECT IndividID, Sex FROM MeerkatViews.qry_LifeHistoryBasic
	WHERE Sex = 'F') C 
    ON B.IndividID = C.IndividID
GROUP BY B.IndividID
ORDER BY NumFiles DESC;


# call-rate
SELECT B.IndividID, sum(CloseCalls), sum(UnlabelledCCs), sum(AggCallEvents), sum(SubCallEvents), sum(RecSubCallEvents), sum(AlarmCallEvents), sum(OtherCallTypes)
FROM KMP_SoundDatabase.tblAnnotationSummary C
JOIN (SELECT D.IndividID, FileID FROM KMP_SoundDatabase.tblSoundFiles D
	JOIN (SELECT IndividID, Sex FROM MeerkatViews.qry_LifeHistoryBasic
		WHERE Sex = 'F') E ON D.IndividID = E.IndividID) B ON C.FileID = B.FileID
GROUP BY B.IndividID;

# litters
SELECT B.IndividID, B.Code, B.LitterName FROM MeerkatViews.qry_LifeHistoryFamily B
JOIN (SELECT D.IndividID, LitterName FROM MeerkatViews.qry_LifeHistoryFamily D
			JOIN (SELECT IndividID, Sex FROM MeerkatViews.qry_LifeHistoryBasic
				WHERE Sex = 'F') E ON D.IndividID = E.IndividID) C
	ON B.LitterName = C.LitterName
WHERE C.IndividID != B.IndividID;

# oldest and newest files
SELECT B.IndividID, min(Date) AS FirstRec, max(Date) AS LastRec, DATEDIFF(max(Date), min(Date))/365 AS DiffTime
FROM KMP_SoundDatabase.tblSoundFiles B
JOIN (SELECT IndividID, Sex FROM MeerkatViews.qry_LifeHistoryBasic
	WHERE Sex = 'F') C 
    ON B.IndividID = C.IndividID
GROUP BY B.IndividID
ORDER BY DiffTime DESC;


# file names
SELECT FileID, FileName, Date, B.IndividID FROM KMP_SoundDatabase.tblSoundFiles B
JOIN (SELECT IndividID, Sex FROM MeerkatViews.qry_LifeHistoryBasic
	WHERE Sex = 'F') C 
    ON B.IndividID = C.IndividID;  

############################################################################################################################################################################
#### selected individuals from Britta Walknehorst's project ####

## select filenames of the indiviudals ##
SELECT IndividID, FileID, FileName, Date, GroupRef, SourceID
FROM KMP_SoundDatabase.tblSoundFiles
WHERE IndividID IN (3242 , 3244 , 3243 , 3241 , 2886 , 2879 , 3188 , 2931 , 2864 , 2986 , 2987 , 1782 , 2985 , 2999 , 2925 , 2997 , 2923 , 2881 , 2392 , 2879 , 2741);
# not many files returned

# look for the ind. in the farm db
SELECT * FROM KMP_FarmDatabase.`Independent Experiments`
WHERE INDIVIDUAL IN ("VRRM181", "VRRM183", "VRRM182", "VRRF180", "VRRF159", "VBBF088" , "VVHM089" , "VBBF093" , "VUKM004", "VUKM017", "VUKF018", "VWM132", "VUKF016", "VZUM016", "VZUM011", "VZUF014", "VZUF009", "VBBM090", "VBBM071", "VBBF088", "VBBF083");
# there are some experiments

# look into the ZH active sound tbl db
SELECT * FROM MeerkatViews.ZH_CompiledActiveSoundTables
WHERE FileName REGEXP 'VRRM181|VRRM183|VRRM182|VRRF180|VRRF159|VBBF088|VVHM089|VBBF093|VUKM004|VUKM017|VUKF018|VWM132|VUKF016|VZUM016|VZUM011|VZUF014|VZUF009|VBBM090|VBBM071|VBBF088|VBBF083';
