from sklearn.model_selection import RandomizedSearchCV
from sklearn.ensemble import RandomForestClassifier
import tensorflow.keras as keras
import tensorflow as tf
from kerastuner import Hyperband, HyperModel
import numpy as np
import librosa
import librosa.display
import os
from IPython.utils import io
import glob
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import cv2
from tensorflow.keras import callbacks


# NEURAL-NET (1 HL MLP)
# build a hypermodel class to tune the ANN
class ANNhypermod(HyperModel):

    def __init__(self, input_shape, n_targets):
        self.input_shape = input_shape
        self.n_targets = n_targets

    def build(self, hp):

        model = keras.Sequential()

        hp_units_input = hp.Int(
            "units_input", min_value=32, max_value=640, step=32)
        hp_units_hidden = hp.Int(
            "units_hidden", min_value=32, max_value=640, step=32)
        hp_activation1 = hp.Choice("activation1",
                                   values=["relu", "softmax",
                                           "sigmoid", "tanh"],
                                   )
        hp_activation2 = hp.Choice("activation2",
                                   values=["relu", "softmax",
                                           "sigmoid", "tanh"],
                                   )
        hp_activation3 = hp.Choice("activation3",
                                   values=["relu", "softmax",
                                           "sigmoid", "tanh"],
                                   )
        model.add(keras.layers.Dense(
            units=hp_units_input, activation=hp_activation1))
        model.add(keras.layers.Dense(
            hp_units_hidden, activation=hp_activation2,
            activity_regularizer="l2"))
        model.add(keras.layers.Dense(20, activation=hp_activation3))

        hp_learning_rate = hp.Choice("learning_rate", values=[
            1e-1, 1e-2, 1e-3, 1e-4])
        model.compile(optimizer=keras.optimizers.Adam(learning_rate=hp_learning_rate),
                      loss="categorical_crossentropy",
                      metrics=['accuracy'])

        return model


# ANN tuning
def ANNtuning(X, y, input_shape):

    # initialize hypermodel
    hypermodel = ANNhypermod(input_shape=input_shape, n_targets=20)

    # initialize tuner
    tuner = Hyperband(
        hypermodel=hypermodel,
        objective="val_accuracy",
        seed=42,
        max_epochs=30,
        project_name="HP_tuning_ANN",
        overwrite=True
    )

    stop_early = keras.callbacks.EarlyStopping(monitor='val_loss', patience=25)

    with io.capture_output() as capture:
        # start searching
        tuner.search(X, y, validation_split=0.2,
                     epochs=50, callbacks=[stop_early], verbose=0)
        best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]

    # Build the model with the optimal hyperparameters
    # and tune epochs and batch size
    BATCH_SIZE = [int(x) for x in np.linspace(start=10, stop=60, num=5)]
    epochs_score = []
    bacth_size_score = []
    best_epochs = []

    for size in BATCH_SIZE:
        model = tuner.hypermodel.build(best_hps)
        history = model.fit(
            X, y,
            epochs=50,
            batch_size=size,
            validation_split=0.4)

        val_acc_per_epoch = history.history['val_accuracy']
        bacth_size_score.append(val_acc_per_epoch)
        epochs_score.append(val_acc_per_epoch)
        best_epochs.append(val_acc_per_epoch.index(max(val_acc_per_epoch)) + 1)

    # visualization of results with # epochs
    colors = ["black", "red", "blue", "green", "cyan"]
    plt.figure()
    for j in range(0, len(BATCH_SIZE)):
        plt.plot(
            range(1, len(bacth_size_score[j]) + 1), bacth_size_score[j],
            color=colors[j], label=BATCH_SIZE[j]
        )
        plt.xlabel("epoch")
        plt.ylabel("validation accuracy")

    plt.legend(loc="best")
    plt.show()
    print("best epochs:", best_epochs)

    # best model
    print("BEST MODEL:")
    print("best number of units input: {}".format(best_hps.get("units_input")))
    print("best number of units hidden: {}".format(best_hps.get("units_hidden")))
    print("best activation input: {}".format(best_hps.get("activation1")))
    print("best activation hidden: {}".format(best_hps.get("activation2")))
    print("best activation outputs: {}".format(best_hps.get("activation3")))
    print("best learning rate: {}".format(best_hps.get("learning_rate")))

    return best_hps, best_epochs


# RF tuning
def RFtuning(X, y):

    rf = RandomForestClassifier(random_state=42)

    # hyper params ranges
    n_estimators = np.arange(start=20, stop=371, step=50)
    criterion = ["gini", "entropy"]
    max_depth = np.arange(start=10, stop=101, step=10)
    min_samples_split = np.arange(start=2, stop=11, step=1)
    max_features = [None, 0.2, 0.5, 0.8, "sqrt", "log2"]
    max_leaf_nodes = [None] + list(range(10, 51, 5))

    # paramn grid
    param_grid = {
        "n_estimators": n_estimators,
        "criterion": criterion,
        "max_depth": max_depth,
        "min_samples_split": min_samples_split,
        "max_features": max_features,
        "max_leaf_nodes": max_leaf_nodes
    }

    # initializing grid search CV object
    rf_search = RandomizedSearchCV(
        estimator=rf,
        param_distributions=param_grid,
        n_jobs=-1,
        cv=5,
        verbose=2,
        n_iter=30
    )

    # fitting
    rf_search.fit(X, y)

    print("RF - Best Params: ")
    print(rf_search.best_params_)
    print("RF - Best Score: ", rf_search.best_score_)

    return rf_search.best_params_, rf_search.best_score_


# CNN hypermodel
class CNNhypermod(HyperModel):
    def __init__(self, input_shape):
        self.input_shape = input_shape

    def build(self, hp):

        hp_filters1 = hp.Int("filters1", 32, 640, step=32)
        hp_filters2 = hp.Int("filters2", 32, 640, step=32)
        hp_filters3 = hp.Int("filters3", 32, 640, step=32)
        hp_units = hp.Int("units", 32, 128, step=32)
        hp_activation1 = hp.Choice("activation1",
                                   values=["relu", "softmax",
                                           "sigmoid", "tanh"],
                                   )
        hp_activation2 = hp.Choice("activation2",
                                   values=["relu", "softmax",
                                           "sigmoid", "tanh"],
                                   )
        hp_activation3 = hp.Choice("activation3",
                                   values=["relu", "softmax",
                                           "sigmoid", "tanh"],
                                   )
        hp_activation4 = hp.Choice("activation4",
                                   values=["relu", "softmax",
                                           "sigmoid", "tanh"],
                                   )
        hp_activation5 = hp.Choice("activation5",
                                   values=["relu", "softmax",
                                           "sigmoid", "tanh"],
                                   )

        # model initialization
        model = keras.models.Sequential()

        # model layers
        model.add(keras.layers.Conv2D(
            filters=hp_filters1, kernel_size=(3, 3),
            activation=hp_activation1, input_shape=self.input_shape
        ))
        model.add(keras.layers.MaxPool2D((2, 2)))

        model.add(keras.layers.Conv2D(
            filters=hp_filters2, kernel_size=(3, 3),
            activation=hp_activation2, activity_regularizer="l2"
        ))
        model.add(keras.layers.MaxPool2D((2, 2)))

        model.add(keras.layers.Conv2D(
            filters=hp_filters3, kernel_size=(3, 3),
            activation=hp_activation3, activity_regularizer="l2"
        ))
        model.add(keras.layers.MaxPool2D((2, 2)))

        model.add(keras.layers.Flatten())
        model.add(keras.layers.Dense(
            units=hp_units, activation=hp_activation4,
            activity_regularizer="l2"
        ))
        model.add(keras.layers.Dense(
            20, activation=hp_activation5))

        # model compile
        hp_learning_rate = hp.Choice("learning_rate", values=[
            1e-1, 1e-2, 1e-3, 1e-4])
        model.compile(optimizer=keras.optimizers.Adam(learning_rate=hp_learning_rate),
                      loss="categorical_crossentropy",
                      metrics=['accuracy'])

        return model


# CNN tuning
def CNNtuning(imgs, y):

    # initialize hypermodel
    hypermodel = CNNhypermod(input_shape=imgs[0].shape)

    # initialize tuner
    tuner = Hyperband(
        hypermodel=hypermodel,
        objective="val_accuracy",
        seed=42,
        max_epochs=10,
        project_name="HP_tuning_CNN",
        overwrite=True
    )

    stop_early = keras.callbacks.EarlyStopping(monitor='val_loss', patience=25)

    # start searching
    with io.capture_output() as capture:
        tuner.search(imgs, y, validation_split=0.2,
                     epochs=50, callbacks=[stop_early], verbose=0)
        best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]

    BATCH_SIZE = [int(x) for x in np.linspace(start=16, stop=64, num=4)]
    epochs_score = []
    bacth_size_score = []
    best_epochs = []

    for size in BATCH_SIZE:
        model = tuner.hypermodel.build(best_hps)
        history = model.fit(
            imgs, y,
            epochs=50,
            batch_size=size,
            validation_split=0.2)

        val_acc_per_epoch = history.history['val_accuracy']
        bacth_size_score.append(val_acc_per_epoch)
        epochs_score.append(val_acc_per_epoch)
        best_epochs.append(val_acc_per_epoch.index(max(val_acc_per_epoch)) + 1)

    # visualization of results with # epochs
    colors = ["black", "red", "blue", "green", "cyan"]
    plt.figure()
    for j in range(0, len(BATCH_SIZE)):
        plt.plot(
            range(1, len(bacth_size_score[j]) + 1), bacth_size_score[j],
            color=colors[j], label=BATCH_SIZE[j]
        )
        plt.xlabel("epoch")
        plt.ylabel("validation accuracy")

    plt.legend(loc="best")
    plt.show()

    print("best epochs:", best_epochs)

    # best model
    print("BEST MODEL:")
    print("best filters(1): {}".format(best_hps.get("filters1")))
    print("best activation(1): {}".format(best_hps.get("activation1")))

    print("best filters(2): {}".format(best_hps.get("filters2")))
    print("best activation(2): {}".format(best_hps.get("activation2")))

    print("best filters(3): {}".format(best_hps.get("filters3")))
    print("best activation(3): {}".format(best_hps.get("activation3")))

    print("best units(4): {}".format(best_hps.get("units")))
    print("best activation(4): {}".format(best_hps.get("activation4")))

    print("best activation(5): {}".format(best_hps.get("activation5")))
    print("best learning rate: {}".format(best_hps.get("learning_rate")))

    return best_hps, val_acc_per_epoch


# audio signal extraction
def audio_extraction(data_dirs):
    # gather all audio filenames
    audio_files = []
    for j in data_dirs:
        audio_files = audio_files + glob.glob(j + "*")

    # indvidual names from file
    ind_name = []
    for i in audio_files:
        ind_name.append(i.split("\\")[-1].split("_")[0])

    # signal extraction
    signals = []

    for i in range(len(audio_files)):
        signal, sr = librosa.load(audio_files[i], sr=44100)
        signals.append(signal)

    return signals, ind_name, audio_files


# feature extraction
def feature_extraction(signals, ind_name, n_mfcc=13, sr=44100, num_windows=15):
    # feature extraction
    X = []

    for i in range(0, len(signals)):

        # setting params
        frame_length = int(round(len(signals[i]) / num_windows))

        # extracting mfccs
        coef = librosa.feature.mfcc(
            signals[i],
            n_mfcc=n_mfcc,
            sr=sr,
            fmax=10000,
            hop_length=frame_length,
            center=True
        )

        # make sure all arrays have the same shape
        if coef.shape[1] == num_windows+1:
            coef = coef[:, :-1]

        # delta coefficients
        delta = librosa.feature.delta(coef, width=5, order=1)
        delta_2 = librosa.feature.delta(coef, width=5, order=2)

        # create dfs for all coefs. excluding first coef
        # and normalization of data first
        coef_norm = (coef[1:, :].T - np.mean(coef[1:, :].T)
                     ) / np.std(coef[1:, :].T)
        delta1_norm = (delta[1:, :].T -
                       np.mean(delta[1:, :].T)) / np.std(delta[1:, :].T)
        delta2_norm = (
            delta_2[1:, :].T - np.mean(delta_2[1:, :].T)) / np.std(delta_2[1:, :].T)

        tmp_df_C = pd.DataFrame(
            coef_norm,
            columns=np.array(["mfcc_" + str(i)
                             for i in np.linspace(1, 14, n_mfcc-1, dtype="int_")])
        )
        tmp_df_D = pd.DataFrame(
            delta1_norm,
            columns=np.array(["D_" + str(i)
                             for i in np.linspace(1, num_windows-1, n_mfcc-1, dtype="int_")])
        )
        tmp_df_D2 = pd.DataFrame(
            delta2_norm,
            columns=np.array(["D2_" + str(i)
                             for i in np.linspace(1, num_windows-1, n_mfcc-1, dtype="int_")])
        )

        # concatenate arrays and put into features list
        X.append(pd.concat([tmp_df_C, tmp_df_D, tmp_df_D2], axis=1).to_numpy())

    # X to numpy array
    X = np.array(X)
    X = X.reshape((X.shape[0], X.shape[1]*X.shape[2]))

    # targets
    individuals = list(set(ind_name))
    targets = []
    for i in ind_name:
        targets.append(individuals.index(i))

    targets = np.array(targets)

    # number of files per ind.
    print("Indiviudals distribution: ")
    print(
        pd.DataFrame({
            "individual": individuals,
            "n": [len(np.where(targets == 0)[0]),
                  len(np.where(targets == 1)[0]),
                  len(np.where(targets == 2)[0]),
                  len(np.where(targets == 3)[0]),
                  len(np.where(targets == 4)[0]),
                  len(np.where(targets == 5)[0]),
                  len(np.where(targets == 6)[0]),
                  len(np.where(targets == 7)[0]),
                  len(np.where(targets == 8)[0]),
                  len(np.where(targets == 9)[0]),
                  len(np.where(targets == 10)[0]),
                  len(np.where(targets == 11)[0]),
                  len(np.where(targets == 12)[0]),
                  len(np.where(targets == 13)[0]),
                  len(np.where(targets == 14)[0]),
                  len(np.where(targets == 15)[0]),
                  len(np.where(targets == 16)[0]),
                  len(np.where(targets == 17)[0]),
                  len(np.where(targets == 18)[0]),
                  len(np.where(targets == 19)[0])]
        })
    )

    return X, targets


# spectrogram extraction
def spec_extract(signals, filenames, ind_name, n_mels=150, sr=44100, n_fft=1024, fmax=10000, frame_stride=0.0001):

    # defining the hop_length
    frame_step = int(sr * frame_stride)

    if len(glob.glob("spectrograms/*")) < len(filenames):
        # extracting the spectrogram images and saving them
        for i, name in enumerate(filenames):
            matplotlib.use("Agg")

            mel_spec = librosa.feature.melspectrogram(
                y=signals[i],
                sr=sr,
                hop_length=frame_step,
                n_mels=n_mels,
                n_fft=n_fft,
                fmax=fmax,
                center=True)
            log_mel_spec = librosa.power_to_db(mel_spec)

            fig = plt.figure(figsize=(4, 2))
            librosa.display.specshow(
                log_mel_spec, sr=sr, fmax=10000,
                x_axis="time", y_axis="mel", cmap="gray_r",
            )
            plt.axis("off")
            fig.savefig("spectrograms/"+name.split("\\")[-1][:-4]+".png",
                        bbox_inches="tight", pad_inches=0)
            plt.close("all")
            del fig

    specs = np.array([np.array(cv2.imread(fyle))
                     for fyle in glob.glob("spectrograms/*")])

    # normalizing pixel values
    specs = specs / 255.0

    # targets
    individuals = list(set(ind_name))
    targets = []
    for i in ind_name:
        targets.append(individuals.index(i))

    targets = np.array(targets)

    return specs, targets


if __name__ == "__main__":
    os.chdir("C:/Users/adelu/Documents/UZH/Thesis/")

    # extract signals
    data_dirs = ["Shared_CallFiles/meerkats/CC/CC_IndividualityWholeGroups_BW/CutAudio_CC_IndividualityWholeGroups_BW/1_Raw/",
                 "Shared_CallFiles/meerkats/CC/CC_IndividualityOldestMF_BW/CutAudio_CC_IndividualityOldestMF_BW/1_Raw/"]
    signals, ind_name, audio_files = audio_extraction(data_dirs)

    specs, targets = spec_extract(
        signals=signals, filenames=audio_files, ind_name=ind_name)

    #best_hps, best_epochs = CNNtuning(imgs=specs, y=targets)
