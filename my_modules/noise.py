"""
Deprecated module to load noise samples and add them to acoustic signals.
Use augment.SigAugment().

Author: Alessandro De Luca
Month/Year: 9/2021
"""


import glob
import numpy as np
import librosa
import random
import soundfile
import pandas as pd


def load_noise(dir):
    """Loads noise signals.

    Args:
        dir: [str] directory in which to find noise files

    Returns:
        noise: [list] Noise signals array
    """
    filenames = glob.glob(dir+"*.wav")

    noise = []

    for i in filenames:
        signal, _ = librosa.load(i, sr=None)

        noise.append(signal)

    return noise


def add_noise(signals, sample_rates, metadata, directory, noise, how_many, remain=0.0):
    """Adds random amount of noise to a signal creating `how_many` augmented signals.

    Adds random amount of noise to a signal creating `how_many` augmented signals.
    Then also saves this signals in the augmented/ directory.

    Args:
        signals: [list] list of signals from librosa.load.
        sample_rates: [list] List of sample rate of each signal.
        metadata: [pd.DataFrame] Metadata dataframe.
        directory: [str] directory where to save the augmented files.
        noise: [list] noise arrays list.
        how_many: [int] How many augmented signals to create.
        remain: [float] Default=0.0. If there is a remainder.

    Returns:
        df: [pd.DataFrane] updated metadata dataframe with augmented file information

    """

    df = metadata.copy(deep=True)

    if not remain:

        for i, sig in enumerate(signals):

            # randomly selected noise samples
            selection = [random.randint(0, len(noise)-1)
                         for j in range(how_many)]
            call_file = metadata["CallFile"].iloc[i][:-4]
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]
            n = 1

            for k in selection:
                # augment
                tmp_noise = np.resize(noise[k], sig.shape)
                tmp_aug = sig + tmp_noise*random.uniform(0.15, 0.35)

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                print("writing " + new_path)

                # save augmented file
                soundfile.write(file=new_path, data=tmp_aug, samplerate=sr)

    else:
        # random signals to augment 1 more time than the others
        idx = [random.randint(0, len(signals)-1)
               for j in range(int(len(signals)*remain))]

        for i, sig in enumerate(signals):

            # randomly selected noise samples
            if i in idx:
                selection = [random.randint(0, len(noise)-1)
                             for j in range(how_many+1)]
            else:
                selection = [random.randint(0, len(noise)-1)
                             for j in range(how_many)]

            call_file = metadata["CallFile"].iloc[i][:-4]
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]
            n = 1

            for k in selection:
                # augment
                tmp_noise = np.resize(noise[k], sig.shape)
                tmp_aug = sig + tmp_noise*random.uniform(0.15, 0.35)

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                print("writing " + new_path)

                # save augmented file
                soundfile.write(file=new_path, data=tmp_aug, samplerate=sr)

    return df
