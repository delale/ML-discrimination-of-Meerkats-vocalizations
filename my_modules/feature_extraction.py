"""
Meerkat close-calls recordings feature extraction module to create:
- MFCCs, delta, delta^2 collection
- mel-spectrogram images
- encoded target labels from individual IDs
- one-hot encoded target labels

Can also filter mask fundamental frequency (f0) or formants (f1...fN).

Author: Alessandro De Luca
Month/Year: 12/2021
"""


import glob
import librosa
import librosa.display
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import cv2
from tensorflow.keras.utils import to_categorical


def audio(audio_files):
    """Fetches and loads .wav files in the listed directories. 

    Takes as argument a list of paths, and loads the .wav files using librosa.

    Args:
        audio_files: [str] paths to audio files

    Returns:
        Tuple (signals, sample_rates)

        signals: [ndarray] list of loaded signals
        sample_rates:[ndarray] list of the sample rates of each signal
    """

    # signal extraction
    signals = []
    sample_rates = []

    # load all signals and sampling-rates
    for i in range(len(audio_files)):
        signal, sr = librosa.load(audio_files[i], sr=None)
        signals.append(signal)
        sample_rates.append(sr)

    return np.array(signals), np.array(sample_rates)


def one_hot(targets, num_classes=None):
    """1-hot encoding of tagets for ANN or CNN.

    Args:
        targets: [ndarray] targets array not 1-hot encoded
        num_classes: [int] default=None. Fix number of classes 
                           (if unique labels < num_classes: one-hot array appened up to num_classes columns with zeros) 

    Returns:
        1-hot encoded targets [ndarray]
    """

    # one-hot encoding
    x = to_categorical(targets)

    # adds zero-arrays for missing clas
    if num_classes and x.shape[1] < num_classes:
        x = np.append(x, np.zeros(
            (x.shape[0], num_classes - x.shape[1])), axis=1)

    return x


def get_targets(metadata_df, show_distribution=False):
    """Takes the individual names and creates a target array for classification.

    Args:
        metadata_df: [pandas.DataFrame] metadata dataframe 
        show_distribution: [bool] Default = False. Show the number of calls per individual

    Returns:
        Tuple (targets, individuals)

        targets: [ndarray] Array of shape (number of signals,) (NOT 1-hot encoded)
        individuals: [set] Set of individuals where index = encoded target.
    """

    # gather individual names
    ind_name = list(metadata_df["Code"])

    individuals = list(set(ind_name))
    individuals.sort()
    targets = []
    # use index of individuals in list to code targets
    for i in ind_name:
        targets.append(individuals.index(i))

    targets = np.array(targets)  # to numpy.array

    if show_distribution:
        # tmp dict to count individual instances
        tmp_dict = {}
        for i in range(len(individuals)):
            tmp_dict[individuals[i]] = len(np.where(targets == i)[0])

        # number of files per ind.
        print("Indiviudals distribution: ")
        print(
            pd.DataFrame({
                "individual": tmp_dict.keys(),
                "n": tmp_dict.values()
            })
        )

    return targets, individuals


def features(signals, sample_rates, n_subsamples=15, n_mels=40, n_mfcc=16, n_fft=1024, frame_stride=0.0001, fmin=100, fmax=5000):
    """Calculates log-power mel-spectrograms, MFCCs, Deltas, Delta-Deltas.

    Args:
        signals: [ndarray] List of signals (from librosa.load()).
        sample_rates: [ndarray] List of sample rate of each signal.
        n_subsamples: [int] Default=15. Number of subsamples to take for each signal.
        n_mels: [int] Default=40. Number of mel-filters.
        n_mfcc: [int] Default=16. Number of MFCCs to extract.
        n_fft: [int] Default=1024. Length of fft window.
        frame_stride: [float] Default=0.0001. Hop length in seconds.
        fmin: [int] Default=100 (working with meerkats). Minimum frequency.
        fmax: [int] Default=5000 (working with meerkats). Maximum frequency.

    Returns:
        Tuple (X, specs_dB).

        X: [ndarray] Features flattened to 2D (MFCCs, Deltas, Delta-Deltas).
        spec_dB: [ndarray] List containing ndarrays of log-power mel-spectrograms.
    """

    specs_dB = np.empty(len(signals), dtype=object)  # log-power spectrum
    X = []  # MFCCs, delta, delta^2

    # for each signal
    for i, signal in enumerate(signals):

        # calculating hop length
        hop = int(round(frame_stride*sample_rates[i]))

        # mel scale spectrogram
        mel_spec = librosa.feature.melspectrogram(
            y=signal, sr=sample_rates[i], n_fft=n_fft, n_mels=n_mels,
            hop_length=hop, fmin=fmin, fmax=fmax
        )

        # log power spectrogram
        S = librosa.power_to_db(mel_spec)

        specs_dB[i] = S  # add to specs_dB array

        # mfcc from log power spectrogram
        mfcc = librosa.feature.mfcc(
            S=S, sr=sample_rates[i], n_mfcc=n_mfcc, n_fft=n_fft,
            hop_length=hop, fmax=fmax, fmin=fmin
        )

        # calculate delta and delta-delta
        D = librosa.feature.delta(mfcc, order=1)
        D2 = librosa.feature.delta(mfcc, order=2)

        # exclude 0th coefficient
        mfcc = mfcc[1:]
        D = D[1:]
        D2 = D2[1:]

        # subsampling
        # raise error if num mfccs < n subsamples
        if mfcc.shape[1] < n_subsamples:
            raise ValueError("array with {} windows is smaller than subsamples {}".format(
                mfcc.shape[1], n_subsamples))

        # random subsample
        subsample = np.random.choice(
            mfcc.shape[1], size=n_subsamples, replace=False)
        subsample.sort()

        # selected subsamples
        sub_mfcc = mfcc[:, subsample]
        sub_D = D[:, subsample]
        sub_D2 = D2[:, subsample]

        # data normalization
        norm_mfcc = (sub_mfcc - np.min(sub_mfcc)) / \
            (np.max(sub_mfcc) - np.min(sub_mfcc))
        norm_D = (sub_D - np.min(sub_D)) / \
            (np.max(sub_D) - np.min(sub_D))
        norm_D2 = (sub_D2 - np.min(sub_D2)) / \
            (np.max(sub_D2) - np.min(sub_D2))

        # concatenate the arrays and append them to features list
        X.append(np.concatenate((norm_mfcc.T, norm_D.T, norm_D2.T), axis=1))

    X = np.array(X)  # to numpy.array

    return X, specs_dB


def spec_img(specs_dB, metadata_df, sample_rates, directory, fmin=100, fmax=5000, created=False, overwrite=False):
    """Creates the spectrogram images and saves them in the parsed directory.

    Args:
        specs_dB: [ndarray] List containing ndarrays of log-power mel-spectrograms.
        metadata_df: [pandas.DataFrame] metadata dataframe.
        sample_rates: [ndarray] List containing the sample rate of each signal.
        directory: [str] Directory where to save the spectrogram images.
        fmin: [int] Default=100 (working with meerkats). Minimum frequency.
        fmax: [int] Default=5000 (working with meerkats). Maximum frequency.
        created [bool] Default=False. Are the images already created?
        overwrite [bool] Default=False. Overwrite already saved images?

    Returns:
        specs: [ndarray] ndarraay of normalized spectrogram images.
        df [pd.DataFrame]: spectrogram dataset (CallFile = path to spectrogram now)
    """
    df = metadata_df.copy(deep=True)

    if not created:

        # only filename without extension
        filenames = [i.split("\\")[-1][:-4]
                     for i in metadata_df["Path"].to_list()]

        # check for already saved images in the directory
        saved_names = [i.split("\\")[-1][:-4]
                       for i in glob.glob(directory + "/*")]

        if (not (set(saved_names) == set(filenames))) or overwrite:

            # create images and save them
            for i, S in enumerate(specs_dB):

                # only those not already saved
                if (filenames[i] not in saved_names) or overwrite:

                    # use Agg to avoid "too many open figures error"
                    matplotlib.use("Agg")

                    # create the img
                    fig = plt.figure(figsize=(4, 2))
                    librosa.display.specshow(
                        S, sr=sample_rates[i],
                        fmax=fmax, fmin=fmin,
                        x_axis="time", y_axis="mel", cmap="gray_r"
                    )
                    plt.axis("off")

                    # save img
                    fig.savefig(
                        directory+"\\"+filenames[i]+".png",
                        bbox_inches="tight", pad_inches=0
                    )

                    # close all + delete figure instance
                    plt.close("all")
                    del fig

                    # modifiy .WAV full path to .png full path (audio->img)
                    df.iloc[i, df.columns.get_loc("Path")] = directory + \
                        "\\" + filenames[i]+".png"
        else:
            print(
                "All images already created... only reading them into arrays and normalizing")

    # reload all images and add them to array
    if not created:

        # to load if not created
        fyles_toload = [directory + "\\" +
                        fyle.split("\\")[-1][:-4] + ".png" for fyle in df["Path"].to_list()]

        df["Path"] = fyles_toload

    else:

        # to load if created (mainly only change .WAV to .png)
        fyles_toload = [fyle[:-4] + ".png" for fyle in df["Path"].to_list()]
        df["Path"] = fyles_toload

    # load imgs and add to specs array
    specs = np.array([np.array(cv2.imread(fyle))
                     for fyle in fyles_toload])

    # normalizing pixel values
    specs = specs / 255.0

    return specs, df


def mask(signals, sample_rates, pass_type):
    """
    Masks frequency ranges depending on the pass type argument.
    Works specifically on meerkat close-calls (no common sub-harmonics).
    Can work on other call types that do not present sub-harmonics or, with small 
    modifications, where the fundamental frequency is the most intense frequency. 

    Args:
        signals: [ndarray] list of loaded signals
        sample_rates: [ndarray] list of the sample rates of each signal
        pass_type: [str] what pass filter to use; options are f0(only fundamental), fN(formants)

    Returns:
        sigs: [ndarray] list of the masked signals
    """

    if not(pass_type in ["f0", "fN"]):
        raise ValueError("pass_type has to be either \"f0\" or \"fN\"")

    sigs = []  # signals

    # pass formants = mask f0
    if pass_type == "fN":
        for i, sig in enumerate(signals):

            sr = sample_rates[i]

            sig_freq = np.fft.rfft(sig)  # sig fft
            sig_freq_abs = np.abs(sig_freq)  # absolute of sig fft

            f0 = np.where(sig_freq_abs == np.max(sig_freq_abs))[
                0] * sr / len(sig)  # fundamental

            where = int(f0 * 1.25 * len(sig) / sr)  # where to mask to

            # mask
            low_filtered = sig_freq.copy()
            low_filtered[:where] = 0.0

            # reverse fft * 2 to return to waveform
            low_filtered = np.fft.irfft(low_filtered) * 2

            sigs.append(low_filtered)

    # pass fundamental = mask f1...fN
    else:
        for i, sig in enumerate(signals):

            sr = sample_rates[i]

            sig_freq = np.fft.rfft(sig)  # sig fft
            sig_freq_abs = np.abs(sig_freq)  # absolute of sig fft

            f0 = np.where(sig_freq_abs == np.max(sig_freq_abs))[
                0] * sr / len(sig)  # fundamental

            where = int(f0 * 1.25 * len(sig) / sr)  # where to mask from

            # mask
            high_filtered = sig_freq.copy()
            high_filtered[where:] = 0.0

            # reverse fft * 2 to return to waveform
            high_filtered = np.fft.irfft(high_filtered) * 2

            sigs.append(high_filtered)

    return np.array(sigs)
