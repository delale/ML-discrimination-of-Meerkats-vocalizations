"""
Module containing different algorithm classes developed for identification of meerkats
through vocalizations (acoustic recordings represented by MFCCs or mel-spectrogram images).
Classes:
- MLP: 1-hudden layer multi-layer perceptron (artificial neural network)
- CNN_1D: convolutional neural network to work on 2D inputs (MFCCs)
- CNN_2D: convolutional neural network to work on 3D inputs (images)
- RandForest: random forest classifier
- Kmeans: k-means clustering classifier
- MultinomReg: multinomial logistic regression model
- Dummy: baseline dummy classifier using a stratified strategy (aka consistent random choice)

Author: Alessandro De Luca
Month/Year: 12/2021
"""


# neural nets
from feature_extraction import features
from scipy.sparse.construct import random
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.layers import Dense, Conv2D, MaxPool2D, Flatten, Conv1D, MaxPool1D
from tensorflow.keras.regularizers import l1_l2, L2
from tensorflow.keras.optimizers import *
from tensorflow.keras.callbacks import EarlyStopping, LearningRateScheduler

# sklearn algorithms
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.dummy import DummyClassifier
from sklearn.cluster import KMeans

# general
import numpy as np
import math
import pickle
import umap

# visualization
import visualkeras
from PIL import ImageFont
from sklearn.tree import export_graphviz
from graphviz import Source
from IPython.display import Image


# Artificial Neural Network
class MLP:
    """
    1-hidden layer Multi-Layer Perceptron class.
    """

    def __init__(self, n_classes, input_shape, random_state, do_umap=True, n_components=45) -> None:
        """
        Model initialization.

        Args:
            n_classes [int]: number of target classes.
            input_shape [tuple]: input array's shape
            random_state [int]: seed for reproducibility.
            do_umap [bool]: default=True; U-Map (n_components)
            n_componente [int]: default=45; num. of u-map components

        Returns:
            None
        """

        self.do_umap = do_umap
        self.n_components = n_components

        # change input_shape if using UMAP to (rows, num_umap_components)
        if self.do_umap:
            input_shape = (input_shape[0], self.n_components)

        tf.random.set_seed(random_state)  # random state

        l1_l2reg = l1_l2(l1=1e-2, l2=1e-2)  # l1_l2 regularization params

        # build model
        self.model = keras.models.Sequential()

        self.model.add(Dense(
            units=input_shape[1], activation="relu"
        ))
        self.model.add(Dense(
            units=128, activation="relu",
            kernel_regularizer=l1_l2reg
        ))
        self.model.add(Dense(
            units=n_classes, activation="softmax"
        ))

        self.model.compile(
            optimizer=Adam(learning_rate=1e-4),
            loss="categorical_crossentropy",
            metrics=["accuracy"]
        )

        self.model.build(input_shape=input_shape)

    def return_model(self):
        """
        Returns keras model
        """
        return self.model

    def fit(self, X, y, batch_size=32, epochs=100):
        """
        Train the model.

        Args:
            X [ndarray]: training features
            y [ndarray]: training targets (one-hot encoded)
            batch_size [int]: default=32; training batch size
            epochs [int]: default=100; number of training epochs

        Returns:
            Training history
        """

        # umap
        if self.do_umap:

            self.reducer = umap.UMAP(
                n_neighbors=100,
                min_dist=0.1,
                n_components=self.n_components,
                metric="manhattan",
                random_state=42,
                target_weight=0.3
            )

            self.reducer.fit(X, y=np.argmax(y, axis=1))  # fit umap
            X = self.reducer.transform(X)  # transorm X to dimensions

        # define learning schedularizer
        def learning_schedule(epoch):
            k = 1e-3
            return 1e-3 * math.exp(-k*epoch)

        lr_schedule = LearningRateScheduler(learning_schedule, verbose=0)

        # early stopping callback
        early_stop = EarlyStopping(
            monitor="val_loss", patience=50, mode="min")

        # fit model
        history = self.model.fit(
            x=X, y=y,
            batch_size=batch_size, epochs=epochs, verbose=2,
            validation_split=0.2, callbacks=[lr_schedule, early_stop]
        )

        return history

    def print_summary(self) -> None:
        """
        Print model summary
        """

        print(self.model.summary())

    def save(self, path) -> None:
        """
        Save model

        Args:
            path [str]: file path (.h5)
        """

        self.model.save(path)

    def load(self, path) -> None:
        """
        Load model

        Args:
            path [str]: file path
        """
        self.model = keras.models.load_model(path)

    def visualize(self, path) -> None:
        """
        Visualize model

        Args:
            path [str]: where to save the file (.png))
            title [str]: figure title
        """

        font = ImageFont.truetype("arial.ttf", 12)
        visualkeras.layered_view(
            self.model, legend=True, font=font, spacing=8,
            draw_volume=False, to_file=path).show()

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        # reduce the dimensions using the trained UMAP (if do_umap)
        if self.do_umap:
            X = self.reducer.transform(X)

        pred_probs = self.model.predict(X)  # predicted class probabilities
        # predicted class labels
        predicted = np.array([np.argmax(x) for x in self.model.predict(X)])

        return predicted, pred_probs


# Convolutional Neural Network (1d)
class CNN_1D:
    """
    Convolutional Neural Network class: 
        - 3 Conv1D layers
        - 3 MaxPool1D layers
        - Flatten layer
        - 1 hidden Dense layer
    """

    def __init__(self, input_shape, n_classes, random_state) -> None:
        """
        Model initialization.

        Args:
            input_shape [tuple]: input array's shape
            n_classes [int]: number of target classes.
            random_state [int]: seed for reproducibility

        Returns:
            None
        """

        tf.random.set_seed(random_state)  # random state

        l2_reg = L2(l2=1e-3)  # l2 regularizer

        # build model
        self.model = keras.Sequential()

        self.model.add(Conv1D(
            filters=512, kernel_size=3,
            activation="relu", input_shape=input_shape,
            activity_regularizer=l2_reg
        ))
        self.model.add(MaxPool1D())
        self.model.add(Conv1D(
            filters=256, kernel_size=3,
            activation="relu",
            activity_regularizer=l2_reg
        ))
        self.model.add(MaxPool1D())
        self.model.add(Flatten())
        self.model.add(Dense(
            units=32, activation="relu",
            activity_regularizer=l2_reg
        ))
        self.model.add(Dense(
            units=n_classes, activation="softmax"
        ))

        self.model.compile(
            optimizer=Adam(learning_rate=1e-4),
            loss="categorical_crossentropy",
            metrics=["accuracy"]
        )

        self.model.build(input_shape=input_shape)

    def return_model(self):
        """
        Returns keras model
        """
        return self.model

    def fit(self, X, y, batch_size=16, epochs=70):
        """
        Train the model.

        Args:
            X [ndarray]: training arrays
            y [ndarray]: training targets (one-hot encoded)
            batch_size [int]: default=16; training batch size
            epochs [int]: default=70; number of training epochs

        Returns:
            Training history
        """

        # def learning_schedule(epoch):
        #     k =1e-4
        #     return 1e-4 * math.exp(-k*epoch)

        # lr_schedule = LearningRateScheduler(learning_schedule, verbose=0)

        # early stopping callback
        early_stop = EarlyStopping(monitor="val_loss", patience=25, mode="min")

        # fit the model
        history = self.model.fit(
            x=X, y=y,
            batch_size=batch_size, epochs=epochs,
            callbacks=[early_stop], verbose=2,
            validation_split=0.2
        )

        return history

    def print_summary(self) -> None:
        """
        Print model summary
        """

        print(self.model.summary())

    def save(self, path) -> None:
        """
        Save model

        Args:
            path [str]: file path (.h5)
        """
        self.model.save(path)

    def visualize(self, path) -> None:
        """
        Visualize model

        Args:
            path [str]: where to save the file (.png))
        """

        font = ImageFont.truetype("arial.ttf", 12)
        visualkeras.layered_view(
            self.model, legend=True, font=font, spacing=8, to_file=path).show()

    def load(self, path) -> None:
        """
        Load model

        Args:
            path [str]: file path
        """

        self.model = keras.models.load_model(path)

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test arrays

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        pred_probs = self.model.predict(X)  # predicted class probabilities
        # predicted class labels
        predicted = np.array([np.argmax(x) for x in self.model.predict(X)])

        return predicted, pred_probs


# Convolutional Neural Network (2d)
class CNN_2D:
    """
    Convolutional Neural Network class: 
        - 3 Conv2D layers
        - 3 MaxPool2D layers
        - Flatten layer
        - 1 hidden Dense layer
    """

    def __init__(self, input_shape, n_classes, random_state) -> None:
        """
        Model initialization.

        Args:
            input_shape [tuple]: input array's shape
            n_classes [int]: number of target classes.
            random_state [int]: seed for reproducibility

        Returns:
            None
        """

        tf.random.set_seed(random_state)  # random state

        l2_reg = L2(l2=1e-2)  # l2 regularizer

        # build model
        self.model = keras.Sequential()

        self.model.add(Conv2D(
            filters=512, kernel_size=(3, 3),
            activation="relu", input_shape=input_shape
        ))
        self.model.add(MaxPool2D((2, 2)))
        self.model.add(Conv2D(
            filters=352, kernel_size=(3, 3),
            activation="relu",
            activity_regularizer=l2_reg
        ))
        self.model.add(MaxPool2D((2, 2))),
        self.model.add(Conv2D(
            filters=256, kernel_size=(3, 3),
            activation="relu",
            activity_regularizer=l2_reg
        ))
        self.model.add(MaxPool2D((2, 2)))
        self.model.add(Flatten())
        self.model.add(Dense(
            units=32, activation="relu",
            activity_regularizer=l2_reg
        ))
        self.model.add(Dense(
            units=n_classes, activation="softmax"
        ))

        self.model.compile(
            optimizer=Adam(learning_rate=1e-4),
            loss="categorical_crossentropy",
            metrics=["accuracy"]
        )

        self.model.build(input_shape=input_shape)

    def return_model(self):
        return self.model

    def fit(self, X, y, batch_size=16, epochs=70):
        """
        Train the model.

        Args:
            X [ndarray]: training arrays
            y [ndarray]: training targets (one-hot encoded)
            batch_size [int]: default=16; training batch size
            epochs [int]: default=70; number of training epochs

        Returns:
            Training history
        """

        # def learning_schedule(epoch):
        #     k =1e-4
        #     return 1e-4 * math.exp(-k*epoch)

        # lr_schedule = LearningRateScheduler(learning_schedule, verbose=0)

        # early stopping callback
        early_stop = EarlyStopping(monitor="val_loss", patience=10, mode="min")

        # fit model
        history = self.model.fit(
            x=X, y=y,
            batch_size=batch_size, epochs=epochs,
            callbacks=[early_stop], verbose=2,
            validation_split=0.2
        )

        return history

    def print_summary(self) -> None:
        """
        Print model summary
        """

        print(self.model.summary())

    def save(self, path) -> None:
        """
        Save model

        Args:
            path [str]: file path (.h5)
        """
        self.model.save(path)

    def visualize(self, path) -> None:
        """
        Visualize model

        Args:
            path [str]: where to save the file (.png))
        """

        font = ImageFont.truetype("arial.ttf", 12)
        visualkeras.layered_view(
            self.model, legend=True, font=font, spacing=8, to_file=path).show()

    def load(self, path) -> None:
        """
        Load model

        Args:
            path [str]: file path
        """

        self.model = keras.models.load_model(path)

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test arrays

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        pred_probs = self.model.predict(X)  # predicted class probabilities
        # predicted class labels
        predicted = np.array([np.argmax(x) for x in self.model.predict(X)])

        return predicted, pred_probs


# Random Forests
class RandForest:
    """
    Random Forest classifier class
    """

    def __init__(self, random_state, do_umap=True, n_components=45) -> None:
        """
        Model initialization.

        Args:
            random_state [int]: seed for reproducibility.
            do_umap [bool]: default=True; U-Map (n_components)
            n_componente [int]: default=45; num. of u-map components

        Returns:
            None
        """

        self.do_umap = do_umap
        self.n_components = n_components

        # build random forest
        self.clf = RandomForestClassifier(
            n_estimators=512, min_samples_split=20,
            max_leaf_nodes=640, max_features="sqrt",
            max_depth=128, criterion="gini",
            random_state=random_state, n_jobs=-1
        )

    def return_model(self):
        """
        Returns model
        """
        return self.clf

    def save(self, path):
        """
        Save model

        Args:
            path [str]: path to file (.pkl)
        """
        # save using PICKLE (.pkl)
        with open(path, "wb") as fb:
            pickle.dump(self.clf, fb, protocol=pickle.HIGHEST_PROTOCOL)

    def feature_importance(self) -> None:
        """
        Prints out feature importance of the RandomForest.
        Use only after having run the fit() method.
        """
        print(self.clf.feature_importances_)

    def visualize(self, path, num_features, labels, jupyter=False):
        """
        Visualization of tree

        Args:
            path [str]: file path (png format)
            num_features [int]: number of features
            labels [list]: class labels
            jupyter [bool]: default=False; show image on jupyter notebook
        """

        tree = Source(export_graphviz(self.clf.estimators_[6], out_file=None,
                                      feature_names=[str(i)
                                                     for i in range(1, num_features+1)],
                                      class_names=labels,
                                      rounded=True, proportion=False,
                                      precision=2, filled=True), format="png"
                      )

        tree.render(filename=path, view=True)

        # jupyter notebook display
        if jupyter:
            Image(path)

    def load(self, path):
        """
        Save model

        Args:
            path [str]: path to file (.pkl)
        """

        # load model using PICKLE (.pkl)
        with open(path, "rb") as fb:
            self.clf = pickle.load(fb)

    def fit(self, X, y):
        """
        Train the model.

        Args:
            X [ndarray]: training features
            y [ndarray]: training targets

        Returns:
            None
        """

        # umap
        if self.do_umap:

            self.reducer = umap.UMAP(
                n_neighbors=100,
                min_dist=0.1,
                n_components=self.n_components,
                metric="manhattan",
                random_state=42,
                target_weight=0.3
            )

            self.reducer.fit(X, y=y)  # fit umap
            X = self.reducer.transform(X)  # transform X dim

        self.clf.fit(X=X, y=y)  # fit random forest

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        # use the trained UMAP on the testing X (if do_umap)
        if self.do_umap:
            X = self.reducer.transform(X)

        predicted = self.clf.predict(X)  # predicted labels
        pred_probs = self.clf.predict_proba(X)  # predicted probabilities

        return predicted, pred_probs


# Multinomial Regression
class MultinomReg:
    """
    Multinomial Regression classifier class
    """

    def __init__(self, random_state, do_umap=True, n_components=45) -> None:
        """
        Model initialization.

        Args:
            random_state [int]: seed for reproducibility.
            do_umap [bool]: default=True; U-Map (n_components)
            n_componente [int]: default=45; num. of u-map components

        Returns:
            None
        """

        self.do_umap = do_umap
        self.n_components = n_components

        # build model
        self.model = LogisticRegression(
            multi_class="multinomial", solver="lbfgs",
            penalty="l2", C=0.9, verbose=0, random_state=random_state, max_iter=10000
        )

    def save(self, path):
        """
        Save model

        Args:
            path [str]: path to file (.pkl)
        """
        # save model using PICKLE (.pkl)
        with open(path, "wb") as fb:
            pickle.dump(self.model, fb, protocol=pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        """
        Save model

        Args:
            path [str]: path to file (.pkl)
        """

        # load model using PICKLE (.pkl)
        with open(path, "rb") as fb:
            self.model = pickle.load(fb)

    def return_model(self):
        """
        Returns model
        """
        return self.model

    def fit(self, X, y):
        """
        Train the model.

        Args:
            X [ndarray]: training features
            y [ndarray]: training targets

        Returns:
            None
        """

        # umap
        if self.do_umap:

            self.reducer = umap.UMAP(
                n_neighbors=100,
                min_dist=0.1,
                n_components=self.n_components,
                metric="manhattan",
                random_state=42,
                target_weight=0.3
            )

            self.reducer.fit(X, y=y)
            X = self.reducer.transform(X)

        self.model.fit(X, y)

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        # use trained UMAP on testing X (if do_umap)
        if self.do_umap:
            X = self.reducer.transform(X)

        predicted = self.model.predict(X)  # predicted labels
        pred_probs = self.model.predict_proba(X)  # predicted probabilities

        return predicted, pred_probs


# K-Means
class Kmeans:
    """
    K-means classifier
    """

    def __init__(self, n_classes, random_state, do_umap=True, n_components=45) -> None:
        """
        Model initialization.

        Args:
            n_classes [int]: number of target classes.
            random_state [int]: seed for reproducibility.
            do_umap [bool]: default=True; U-Map (n_components)
            n_componente [int]: default=45; num. of u-map components

        Returns:
            None
        """

        self.do_umap = do_umap
        self.n_components = n_components

        # build model
        self.clf = KMeans(
            n_clusters=n_classes, init="k-means++", n_init=50,
            max_iter=500, random_state=random_state, algorithm="full"
        )

    def return_model(self):
        """
        Returns model
        """
        return self.clf

    def save(self, path):
        """
        Save model

        Args:
            path [str]: path to file (.pkl)
        """

        # save model using PICKLE (.pkl)
        with open(path, "wb") as fb:
            pickle.dump(self.clf, fb, protocol=pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        """
        Save model

        Args:
            path [str]: path to file (.pkl)
        """

        # load model using PICKLE (.pkl)
        with open(path, "rb") as fb:
            self.clf = pickle.load(fb)

    def fit(self, X, y):
        """
        Train the model.

        Args:
            X [ndarray]: training features

        Returns:
            None
        """

        # umap
        if self.do_umap:

            self.reducer = umap.UMAP(
                n_neighbors=100,
                min_dist=0.1,
                n_components=self.n_components,
                metric="manhattan",
                random_state=42,
                target_weight=0.3
            )

            self.reducer.fit(X, y=y)
            X = self.reducer.transform(X)

        self.clf.fit(X)  # fit model

        # Matching of clusters to true targets for later use in testing
        # to transform the predicted clusters to predicted labels.
        # Cluster-Label match using a majority rule, where the training label
        # that is most common in each cluster is selected as the cluster's label.
        # Not necessarily are all labels assigned to a cluster (if a label is
        # never the majority in any cluster it will not have a cluster assigned).

        labels = self.clf.labels_  # cluster labels
        self.clusters_dict = {}  # cluster dict {cluster: class_label}

        for k in np.unique(labels):
            # sum of all matches of the true targets for each cluster
            match = [np.sum((labels == k) * (y == t)) for t in np.unique(y)]

            # cluster assigned to label for which matches sum is max
            self.clusters_dict[k] = np.unique(y)[np.argmax(match)]

    def predict(self, X, y_test):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features
            y_test [ndarray]: true test targets (used for most-likely cluster classification)

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        # trained UMAP for testing X (if do_umap)
        if self.do_umap:
            X = self.reducer.transform(X)

        k_labels = self.clf.predict(X)  # predicted clustery

        predicted = np.empty_like(k_labels)  # empty array for predicted labels
        pred_probs = np.empty(
            shape=(len(k_labels), len(np.unique(y_test))),
            dtype=float
        )  # empty array for predicted class labels

        for k in np.unique(k_labels):
            # predicted label = to training cluster label
            predicted[k_labels == k] = self.clusters_dict[k]

        for k in np.unique(k_labels):
            match = [np.sum((k_labels == k) * (predicted == t))
                     for t in np.unique(y_test)]
            pred_probs[k_labels == k] = match/np.sum(match)

        return predicted, pred_probs


# Dummy Classifier
class Dummy:
    """
    Baseline Dummy classifier (using stratified strategy).
    Represents essentially a consistent random choice classifier 
    and can be used for comparisons with other classifiers by acting as a baseline.
    """

    def __init__(self, random_state) -> None:
        """
        Model initialization.

        Args:
            random_state [int]: seed for reproducibility.

        Returns:
            None
        """

        # build model
        self.model = DummyClassifier(
            strategy="stratified",
            random_state=random_state
        )

    def save(self, path):
        """
        Save model

        Args:
            path [str]: üath to file (.pkl)
        """

        with open(path, "wb") as fb:
            pickle.dump(self.model, fb, protocol=pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        """
        Save model

        Args:
            path [str]: path to file (.pkl)
        """

        with open(path, "rb") as fb:
            self.model = pickle.load(fb)

    def fit(self, X, y):
        """
        Train the model.

        Args:
            X [ndarray]: training features
            y [ndarray]: training targets

        Returns:
            None
        """

        self.model.fit(X=X, y=y)

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        predicted = self.model.predict(X=X)  # predicted labels

        pred_probs = self.model.predict_proba(X=X)  # predicted probabilities

        return predicted, pred_probs


if __name__ == "__main__":

    import os
    import pandas as pd
    import feature_extraction
    import numpy as np

    # testing all methods
    os.chdir("C:/Users/alessandro/Documents/UZH/Thesis/")

    testing = pd.read_csv("code/general_identification/datasets/testing.csv")

    sig, sr = feature_extraction.audio(testing.Path.to_list())

    X, _ = feature_extraction.features(signals=sig, sample_rates=sr)

    X = np.reshape(X, newshape=(X.shape[0], X.shape[1]*X.shape[2]))

    y, ind = feature_extraction.get_targets(testing)

    rf = RandForest(random_state=42)

    rf.fit(X=X, y=y)

    rf.save("rf.pkl")

    rf.load("rf.pkl")

    rf.visualize("rf.dot", num_features=675, labels=ind)
