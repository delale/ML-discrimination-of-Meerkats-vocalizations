"""
Module to evalate performance of classifiers on identification tasks.
Possible metrics to compute:
- ROC curves (both class-wise and micro-avg. precision).
- confusion matrix
- Cohen's kappa
- Matthews correlation coefficient
- F1 score
- AUC
- precision and recall (class-wise and macro-avg.)
- accuracy

Statistical test: McNemar's test of agreement between classifiers (can also compute contingency table)

Author: Alessandro De Luca
Month/Year: 12/2021
"""


import matplotlib
from sklearn.metrics import (
    confusion_matrix, matthews_corrcoef, cohen_kappa_score, recall_score, auc,
    precision_score, roc_auc_score, roc_curve, accuracy_score, f1_score
)
from statsmodels.stats.contingency_tables import mcnemar
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from tensorflow.python.keras.backend import square


def cohen(y, yp):
    """
    Cohen's kappa score

    Args:
        y [ndarray]: true targets (n_samples, )
        yp [ndarray]: predicted targets (n_samples, )

    Returns:
        Cohen's kappa
    """

    return cohen_kappa_score(y1=y, y2=yp)


def matthews(y, yp):
    """
    Matthews Correlation Coefficient

    Args:
        y [ndarray]: true targets (n_samples, )
        yp [ndarray]: predicted targets (n_samples, )

    Returns:
        matthews_corrcoef [float]: Matthews Correlation Coefficient
    """

    return matthews_corrcoef(y_true=y, y_pred=yp)


def conf_matrix(y, yp, labels, matthews_corr=None, cohens=None, figsize=(7, 7), font_scale=0.5, show_figure=True, ax=None):
    """
    Confusion Matrix + Figure

    Args:
        y [ndarray]: true targets (n_samples, )
        yp [ndarray]: predicted targets (n_samples, )
        labels [list]: class labels
        matthews_corr [float]: default=None; Matthews Correlation Coefficient to print with the figure
        cohens [float]: default=None; Cohen's kappa score
        figsize [tuple]: default=(7, 10); figure size
        font_scale [float]: default=0.5; font size scaling of labels
        show_figure [bool]: default=True; show the figure or just return it
        ax: default=None; figure axes

    Returns:

        cf [sklearn confusion_matrix]: confusion matrix,
        fig: figure
    """

    sns.set(font_scale=font_scale)

    # compute confusion matrix
    cf = confusion_matrix(y_true=y, y_pred=yp, normalize="true")

    # plot confusion matrix
    if ax is None:
        plt.figure(figsize=figsize)
        ax = plt.gca()
    matplotlib.rc("axes", titlesize=20)
    fig = sns.heatmap(cf, xticklabels=labels,
                      yticklabels=labels, annot=True, fmt=".2f", ax=ax,
                      cmap=sns.color_palette("viridis", as_cmap=True),
                      square=True, cbar_kws={"shrink": .8})
    ax.set_ylabel("True")

    # add metric labels
    if matthews_corr:
        ax.set_xlabel("Predicted" + "\nMCC: {:0.3f}".format(matthews_corr))
    elif cohens:
        ax.set_xlabel("Predicted" + "\nKappa: {:0.3f}".format(cohens))
    else:
        ax.set_xlabel("Predicted")

    if show_figure:
        plt.show()

    return cf, fig


def metrics(y, yp, y_true, y_pred, labels):
    """
    Calculates metrics: 
        - Macro avg. Accuracy 
        - Precision and Recall (both classwise and micro avg.) 

    Args:
        y [ndarray]: true targets (n_samples, )
        yp [ndarray]: predicted targets (n_samples, )
        y_true [ndarray]: true targets (n_samples, n_classes)
        y_pred [ndarray]: predicted targets (n_samples, n_classes)
        labels [list]: class labels

    Returns:

        metr [pd.DataFrame]: Precision and Recall metrics
        acc [float]: micro avg. Accuracy
        f1 [float]: macro avg. F1 score
    """

    class_rec = []
    class_prec = []

    # classwise metrics
    for i in range(len(labels)):
        rec = recall_score(y_true=y_true[:, i], y_pred=y_pred[:, i])
        prec = precision_score(y_true=y_true[:, i], y_pred=y_pred[:, i])

        class_rec.append(rec)
        class_prec.append(prec)

    # average metrics
    f1 = f1_score(y_true=y, y_pred=yp, average="macro")
    acc = accuracy_score(y_true=y, y_pred=yp)
    rec = recall_score(y_true=y, y_pred=yp, average="macro")
    prec = precision_score(y_true=y, y_pred=yp, average="macro")
    class_rec.append(rec)
    class_prec.append(prec)

    metr = pd.DataFrame(
        {
            "Label": labels + ["Macro Avg."],
            "Precision": class_prec,
            "Recall": class_rec
        }
    )

    return metr, acc, f1


def roc_auc(y, y_true, y_score, n_classes, figsize=(7, 7), show_figure=True, ax=None):
    """
    ROC curves classwise and micro avg. + one-vs-rest AUC score

    Args:
        y [ndarray]: true targets (n_samples, )
        y_true [ndarray]: true targets (n_samples, n_classes)
        y_score [ndarray]: predicted probabilities for each class (n_samples, n_classes)
        n_classes [int]: number of classes
        figsize [tuple]: default=(7, 7); figure size
        show_figure [bool]: default=True; show the figure or just return it
        ax: default=None; figure axes

    Returns:

        auc_score [float]: one-vs-rest AUC score
        fig: figure
    """

    # colors for each class (max 20)
    colors = [
        '#3ed1b7', '#dbafe3', '#da7f07', '#4546e3', '#32b7fa',
        '#28947f', '#883030', '#34b6e9', '#a0c757', '#b3dc0d',
        '#3ac053', '#a63ae6', '#29b10f', '#c1ca0f', '#85dd83',
        '#998cf8', '#389beb', '#09b900', '#755378', '#fe46fe'
    ]

    # plot ROC
    if ax is None:
        plt.figure(figsize=figsize)
        ax = plt.gca()

    for i in range(n_classes):
        fpr, tpr, _ = roc_curve(y_true=y_true[:, i], y_score=y_score[:, i])

        fig, = ax.plot(fpr, tpr, color=colors[i])

    fpr, tpr, _ = roc_curve(y_true=y_true.ravel(), y_score=y_score.ravel())
    fig = ax.plot(fpr, tpr,
                  linestyle="dashdot", color="red", linewidth=3,
                  label="Micro Avg."
                  )
    fig = ax.plot([0, 1], [0, 1], "k--")
    ax.set_xlabel("False Positive Rate")
    ax.set_ylabel("True Positive Rate")
    ax.legend(loc="lower right")

    if show_figure:
        plt.show()

    # compute AUC
    auc_score = roc_auc_score(y_true=y, y_score=y_score, multi_class="ovr")

    return auc_score, fig


def mcnemar_test(y_true, clf1_pred, clf2_pred, print_contingency=False):
    """
    McNemar's test for comparison of classifiers

    Args:
        y_true [ndarray]: true targets (n_samples, )
        clf1_pred [ndarray]: predicted targets of the first classifier (n_samples, )
        clf2_pred [ndarray]: predicted targets of the second classifier (n_samples, )
        print_contingency [bool]: default=False; if True print the contingency table

    Returns:
        stat [float]: McNemar's test statistic
        p [float]: p-value
        tab [pd.Crosstab]: contingency table
    """

    # contingency table frequencies (TRUE/FALSE) for each classifier
    clf1 = np.equal(y_true, clf1_pred)
    clf2 = np.equal(y_true, clf2_pred)

    # contingency table
    tab = pd.DataFrame({
        "clf1": clf1,
        "clf2": clf2
    })
    tab = pd.crosstab(tab["clf1"], tab["clf2"])

    if print_contingency:
        print("Contingency table:\n")
        print(tab)

    # McNemar's test
    results = mcnemar(tab, exact=False, correction=True)

    return results.statistic, results.pvalue, tab
