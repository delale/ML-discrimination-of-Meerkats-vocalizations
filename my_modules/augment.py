"""
Data augmentation script for both augmenting acoustic signals as well as spectrogram images.
- acoustic signals: uses noise injection and/or time shifting of the signal¨
- spectrogram images: uses SpecAugment framework masking on the frequency and/or time domain.

Author: Alessandro De Luca
Month/Year: 12/2021
"""


import pandas as pd
import numpy as np
import librosa
import librosa.display
import soundfile
import glob
import matplotlib
import matplotlib.pyplot as plt


class augment_audio:

    def __init__(self,  directory) -> None:
        """
        Initialization of augmenter for signals using noise.

        Args:
            directory [str]: directory of noise samples
        """
        filenames = glob.glob(directory+"*.wav")

        self.noise = []

        # load noise signals and append them to nouise list
        for i in filenames:
            signal, _ = librosa.load(i, sr=None)

            self.noise.append(signal)

    def SigAugment(self, signals, sample_rates, metadata, directory, N, seed=42, aug_type="both"):
        """
        Adds random amount of noise or shifts (or both) a signal creating `how_many` augmented signals.
        Then also saves this signals in the augmented/ directory.

        Args:
            signals: [list] list of signals from librosa.load
            sample_rates: [list] list of sample rate of each signal
            metadata: [pd.DataFrame] metadata dataframe
            directory: [str] directory where to save the augmented files
            N: [int] how many signals
            seed [int]: default=42; random seed for reproducibility
            aug_type [str]: default="both"; type of augmentation:
                            noise(adds only noise), shift (only shifts), both (default), random (random choice between the other options for each augmentation)

        Returns:
            df: [pd.DataFrane] updated metadata dataframe with augmented file information
        """

        # raise ValueError if invalid aug_type
        if not(aug_type in ["noise", "shift", "both", "random"]):
            raise ValueError(
                "aug_type value does not correspond to any of the available options")

        # set is_random to Tue if aug_type is random (randomly choose every augmentation)
        if aug_type == "random":
            is_random = True
        else:
            is_random = False

        df = metadata.copy(deep=True)  # copy dataframe

        np.random.seed(seed)  # random seed

        n_calls = metadata.shape[0]  # number of original calls

        # how many augmented signals per original call
        how_many = (N-n_calls)//n_calls

        # if no new calls needed
        if how_many == 0:
            return df

        j = 0  # call counter (all calls)

        for i, sig in enumerate(signals):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]

            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]

            sr = sample_rates[i]  # sampling rate of the signal

            n = 1  # sequential numbering of augmented signals

            j += 1  # keep count also of original signals

            # random selection of noise
            selection = [np.random.randint(
                0, len(self.noise) - 1) for j in range(how_many)]

            # for each noise sample (==how_many times)
            for k in selection:

                # if aug_type=="random" choose random aug_type for each augmentation
                if is_random:
                    aug_type = np.random.choice(["noise", "shift", "both"])

                if aug_type == "noise":
                    # resize noise to siganl dims
                    tmp_noise = np.resize(self.noise[k], sig.shape)

                    # augment signal
                    augmented = sig.copy()
                    augmented = augmented + tmp_noise

                    # metadata rewrite
                    tmp_series["CallFile"] = call_file + \
                        "_aug_{:02}".format(n) + ".WAV"  # new call-file
                    new_path = directory + "\\" + call_file + \
                        "_aug_{:02}".format(n) + ".WAV"  # new path
                    tmp_series["Path"] = new_path
                    n += 1  # increase seq. numbering

                    df = df.append(tmp_series)  # append to original dataframe

                    # save augmented file
                    soundfile.write(
                        file=new_path, data=augmented, samplerate=sr
                    )

                    j += 1  # increase call counter

                elif aug_type == "shift":
                    # get random shift:
                    s = int(np.random.choice(
                        [-1, 1]) * np.random.uniform(0.1, 0.3) * len(sig))

                    # augment signal
                    augmented = sig.copy()
                    augmented = np.roll(augmented, s)

                    # padding
                    if s > 0:
                        augmented[:s] = 0
                    else:
                        augmented[s:] = 0

                    # metadata rewrite
                    tmp_series["CallFile"] = call_file + \
                        "_aug_{:02}".format(n) + ".WAV"  # new call-file
                    new_path = directory + "\\" + call_file + \
                        "_aug_{:02}".format(n) + ".WAV"  # new path
                    tmp_series["Path"] = new_path
                    n += 1  # increase seq. numbering

                    df = df.append(tmp_series)  # append to original dataframe

                    # save augmented file
                    soundfile.write(
                        file=new_path, data=augmented, samplerate=sr)

                    j += 1  # increase call counter

                elif aug_type == "both":
                    # resize noise to siganl dims
                    tmp_noise = np.resize(self.noise[k], sig.shape)

                    # augment signal w/ noise
                    augmented = sig.copy()
                    augmented = augmented + tmp_noise

                    # get random shift:
                    s = int(np.random.choice(
                        [-1, 1]) * np.random.uniform(0.1, 0.3) * len(sig))

                    # augment signal w/ shift
                    augmented = np.roll(augmented, s)

                    # padding
                    if s > 0:
                        augmented[:s] = 0
                    else:
                        augmented[s:] = 0

                    # metadata rewrite
                    tmp_series["CallFile"] = call_file + \
                        "_aug_{:02}".format(n) + ".WAV"  # new call-file
                    new_path = directory + "\\" + call_file + \
                        "_aug_{:02}".format(n) + ".WAV"  # new path
                    tmp_series["Path"] = new_path
                    n += 1  # increase seq. numbering

                    df = df.append(tmp_series)  # append to original dataframe

                    # save augmented file
                    soundfile.write(
                        file=new_path, data=augmented, samplerate=sr)

                    j += 1  # increase call counter

        d = {}  # dictionary for correct sequential numbering of augmented calls

        # augment random signals again to reach N
        while j < N:

            # random original call
            i = np.random.randint(0, metadata.shape[0] - 1)

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]

            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]

            sig = signals[i]  # signal
            sr = sample_rates[i]  # sampling rate of the signal

            # sequential numbering (start from last augmented)
            if not(call_file in d.keys()):
                n = how_many + 1
                d["call_file"] = [n]
            else:
                n = how_many + 1 + len(d["call_file"])
                d["call_file"].append(n)

            # select random noise sample
            tmp_noise = self.noise[np.random.randint(0, len(self.noise)-1)]

            # resize noise to siganl dims
            tmp_noise = np.resize(tmp_noise, sig.shape)

            # if aug_type=="random" choose random aug_type for each augmentation
            if is_random:
                aug_type = np.random.choice(["noise", "shift", "both"])

            if aug_type == "noise":
                # resize noise to siganl dims
                tmp_noise = np.resize(self.noise[k], sig.shape)

                # augment signal
                augmented = sig.copy()
                augmented = augmented + tmp_noise

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"  # new call-file
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"  # new path
                tmp_series["Path"] = new_path
                n += 1  # increase seq. numbering

                df = df.append(tmp_series)  # append to original dataframe

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr
                )

                j += 1  # increase call counter

            elif aug_type == "shift":
                # get random shift:
                s = int(np.random.choice(
                    [-1, 1]) * np.random.uniform(0.1, 0.3) * len(sig))

                # augment signal
                augmented = sig.copy()
                augmented = np.roll(augmented, s)

                # padding
                if s > 0:
                    augmented[:s] = 0
                else:
                    augmented[s:] = 0

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"  # new call-file
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"  # new path
                tmp_series["Path"] = new_path
                n += 1  # increase seq. numbering

                df = df.append(tmp_series)  # append to original dataframe

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr)

                j += 1  # increase call counter

            elif aug_type == "both":
                # resize noise to siganl dims
                tmp_noise = np.resize(self.noise[k], sig.shape)

                # augment signal w/ noise
                augmented = sig.copy()
                augmented = augmented + tmp_noise

                # get random shift:
                s = int(np.random.choice(
                    [-1, 1]) * np.random.uniform(0.1, 0.3) * len(sig))

                # augment signal w/ shift
                augmented = np.roll(augmented, s)

                # padding
                if s > 0:
                    augmented[:s] = 0
                else:
                    augmented[s:] = 0

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"  # new call-file
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"  # new path
                tmp_series["Path"] = new_path
                n += 1  # increase seq. numbering

                df = df.append(tmp_series)  # append to original dataframe

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr)

                j += 1  # increase call counter

        return df


class augment_specs:

    def __init__(self) -> None:
        pass

    def SpecAugment(self, specs, metadata, directory, N, aug_type="both", max_mask=0.1, seed=42):
        """
        Spectrogram augmentation using SpecAugment framework (only masking, no time-warping)

        Args:
            specs [ndarray]: list of spectrogram images
            metadata [pd.DataFrame]: metadata dataframe
            directory [str]: directory where to save augmented spectrogram images
            N [int]: how many files in total
            aug_type [str]: what type of SpecAugment to do; 
                            options are freq (freq. masking), time (time masking), both (default), random (random choice between the other options for each augmentation).
            max_mask [float]: Default=0.1; maximum width of mask expressed as ratio of image dimensions.
            seed [int]: random seed for reproducibility

        Returns:
            df [pd.DataFrame]: augmented dataframe
        """

        # raise ValueError if invalid aug_type
        if not(aug_type in ["freq", "time", "both", "random"]):
            raise ValueError(
                "aug_type value does not correspond to any of the available options")

        # set is_random to Tue if aug_type is random (randomly choose every augmentation)
        if aug_type == "random":
            is_random = True
        else:
            is_random = False

        df = metadata.copy(deep=True)  # dataframe copy

        np.random.seed(seed)  # random seed

        n_calls = metadata.shape[0]  # number of original calls

        # how many augmented signals per original call
        how_many = (N-n_calls)//n_calls

        j = 0  # call counter

        n_cols = specs[0].shape[1]  # num of columns
        n_rows = specs[0].shape[0]  # num of rows

        for i, S in enumerate(specs):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]

            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]

            n = 1  # sequential numbering of augmented signals

            j += 1  # keep count also of original signals

            for k in range(how_many):

                # if aug_type == "random" randomly choose aug_type
                if is_random:
                    aug_type = np.random.choice(["freq", "time", "both"])

                if aug_type == "time":
                    # randomly select where to start mask
                    start = np.random.randint(
                        0, n_cols - int(max_mask*n_cols)-1)
                    end = start + int(max_mask*n_cols)

                    # mask selection
                    augmented = S.copy()
                    augmented[:, start:end+1, :] = 0.0

                if aug_type == "freq":
                    # randomly select where to start mask
                    start = np.random.randint(
                        0, n_rows - int(max_mask*n_rows)-1)
                    end = start + int(max_mask*n_rows)

                    # mask selection
                    augmented = S.copy()
                    augmented[start:end+1, :, :] = 0.0

                if aug_type == "both":
                    # randomly select where to start mask
                    start = np.random.randint(
                        0, n_cols - int(max_mask*n_cols)-1)
                    end = start + int(max_mask*n_cols)

                    # mask columns (time)
                    augmented = S.copy()
                    augmented[:, start:end+1, :] = 0.0

                    # randomly select where to start mask
                    start = np.random.randint(
                        0, n_rows - int(max_mask*n_rows)-1)
                    end = start + int(max_mask*n_rows)

                    # mask rows (freq)
                    augmented[start:end+1, :, :] = 0.0

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".png"  # new call-file
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".png"  # new path
                tmp_series["Path"] = new_path
                n += 1  # increase seq. numbering

                # save image
                matplotlib.image.imsave(new_path, augmented, cmap="gray")

                j += 1  # increase call counter

                df = df.append(tmp_series)  # append to dataframe

        d = {}  # dictionary for correct sequential numbering of augmented calls

        # augment random signals again to reach N
        while j < N:

            # random original call
            i = np.random.randint(0, metadata.shape[0] - 1)

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]

            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            S = specs[i]  # spectrogram

            # sequential numbering (start from last augmented)
            if not(call_file in d.keys()):
                n = how_many + 1
                d["call_file"] = [n]
            else:
                n = how_many + 1 + len(d["call_file"])
                d["call_file"].append(n)

            # if aug_type == "random" randomly choose aug_type
            if is_random:
                aug_type = np.random.choice(["freq", "time", "both"])

            if aug_type == "time":
                # randomly select where to start mask
                start = np.random.randint(
                    0, n_cols - int(max_mask*n_cols)-1)
                end = start + int(max_mask*n_cols)

                # mask selection
                augmented = S.copy()
                augmented[:, start:end+1, :] = 0.0

            if aug_type == "freq":
                # randomly select where to start mask
                start = np.random.randint(
                    0, n_rows - int(max_mask*n_rows)-1)
                end = start + int(max_mask*n_rows)

                # mask selection
                augmented = S.copy()
                augmented[start:end+1, :, :] = 0.0

            if aug_type == "both":
                # randomly select where to start mask
                start = np.random.randint(
                    0, n_cols - int(max_mask*n_cols)-1)
                end = start + int(max_mask*n_cols)

                # mask columns (time)
                augmented = S.copy()
                augmented[:, start:end+1, :] = 0.0

                # randomly select where to start mask
                start = np.random.randint(
                    0, n_rows - int(max_mask*n_rows)-1)
                end = start + int(max_mask*n_rows)

                # mask rows (freq)
                augmented[start:end+1, :, :] = 0.0

            # metadata rewrite
            tmp_series["CallFile"] = call_file + \
                "_aug_{:02}".format(n) + ".png"  # new call-file
            new_path = directory + "\\" + call_file + \
                "_aug_{:02}".format(n) + ".png"  # new path
            tmp_series["Path"] = new_path
            n += 1  # increase seq. numbering

            # save image
            matplotlib.image.imsave(new_path, augmented, cmap="gray")

            j += 1  # increase call counter

            df = df.append(tmp_series)  # append to dataframe

        return df
