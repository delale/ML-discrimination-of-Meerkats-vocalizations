# **Comparison of Machine Learning Methods for Vocal Identification of Meerkats (*Suricata suricatta*)**

# Table of contents
1. [Summary](#1-Summary)  
2. [Methods](#2-Methods)  
    2.1 [Workflow & Datasets](##2.1-Workflow-&-Datasets)  
    2.2 [Feature extraction & Data augmentation](##2.2-Feature-extraction-&-Data-augmentation)  
    2.3 [Algorithms](##2.3-Algorithms)  
    2.4 [Statistical evaluation](##2.4-Statistical-evaluation)  
    2.5 [Study site & population](##2.5-Study-site-&-population)
3. [Results](#3-Results)  
    3.1 [ID1](##3.1-ID1)  
    3.2 [ID2](##3.2-ID2)  
    3.3 [ID3]([##3.3-ID3])  
4. [Acknowledgements](#Acknowledgements)

# 1 Summary
In this project I compared different vocal identification tools to vocally distinguish meerkats. I investigated which type of feature extraction yielded the best classification performance, and with which algorithm, while keeping in mind processing times. The best algorithm out of the ones compared was a Random Forest using mel-spectrogram images as input data rather then mel-frequency cepstrum coefficients (MFCCs). I also investigated the influene of context in which the vocalization is produced and the stability of the inidividual signature of meerkats. Context did slightly influence classification performance, which I suggest is due to differences in noise-to-signal ratio between the two contexts compared. Age at the recording did not influence the performance of the algorithm.

# 2 Methods
## 2.1 Workflow & Datasets
Three identification tasks were used for the analysis:
- ID1: general comparison of algorithm performance [*general_identification*](general_identification)  
![ID1 workflow](pictures/methods/workflow_ID1.png)  
- ID2: influence of context [*context_analysis*](context_analysis)
![ID1 workflow](pictures/methods/workflow_ID2.png)  
*(SC = scrabbling, DI = digging, MIXED = DI + SC)*
- ID3: influence of individuals’ age [*stability_analysis*](stability_analysis)
![ID1 workflow](pictures/methods/workflow_ID3.png)  
*(juveniles/subadults [3-12 month], yearlings [12-24], adults [>24])*

[*Datasets description*](tables/methods/datasets_description.csv)  
[*Number of files and extracted calls per individual*](tables/methods/number_of_files.csv)

## 2.2 Feature extraction & Data augmentation
All the feature extraction was done in Python using *librosa* (*my_modueles/feature_extraction.py*)  
Data augmentation followed noise injection and time shifting (Nanni et al., 2020) for the raw audio files before MFCCs extraction and SpecAugment (Park et al. 2019) [*my_modueles/augment.py*](my_modules/augment.py):  
e.g. of mel-spectrogram image before and after augmentation:  
<img src="pictures/methods/specaugment.png"
    alt="SpecAugment_example"
    width="918"
    height="214" />  

## 2.3 Algorithms
All the algorithms classes can be found in [*my_modules/algorithms.py*](my_modules/algorithms.py)  
- Baseline dummy classifier
- K-means clustering
- Multinomial logistic regression
- Random forest
- 1-hidden layer multi-layer perceptron (MLP/ANN)
- Convolutional neural network (1D CNN for MFCCs, 2D CNN for Spec. Img.s)

## 2.4 Statistical evaluation
[*my_modules/evaluate_performance.py*](my_modules/evaluate_performance.py)  
Visual evaluation:
- confusion matrices (Stehman, 1997) 
- receiver operating characteristics (ROC) curves (Zweig & Campbell,1993) (class-wise and micro averaged)  
Numerical evaluation:
- Matthew’s correlation coefficient (MCC) (Matthews, 1975)
- precision and recall (class-wise and macro average)
- area under the curve (AUC) score (Hanley & McNeil, 1982)
Statistical tests:
- McNemar’s test (McNemar, 1947)
- Welch’s t-test (Welch, 1947); effect size: Cohen’s d (Cohen, 1988)
- Welch’s ANOVA (Welch, 1951); effect size: partial η2 (Cohen, 1988)

## 2.5 Study site & poulation
Acoustic recordings gathered at the Kalahari Research Centre (KRC) in the Kuruman River Reserve, South Africa (26°58’S, 21°49’E) as part of the Kalahari Meerkat Project (KMP) long-term study (Clutton-Brock et al., 1999; Clutton-Brock & Manser, 2016) between 2014 and 2017.   
![KRC](pictures/methods/krc.png)  
20 meerkats from 6 groups from a wild habituated population:
- close to 1:1 ± 1 individual [*table of sex distribution*](tables/methods/sex_distribution.csv)
- 3 individuals moved groups during the recording period [*tabel of individuals which moved groups*](tables/methods/moved_groups.csv)

# 3 Results
## 3.1 ID1
[*table of average metrics results*](tables/results/ID1_avg_metrics.csv)  
- Random forest (using mel-spectrogram images) best classifier
![Random_forest_conf_matrix](pictures/results/rf_spec_confusion_matrix.png)  
- No difference between MFCCs and mel-spectrogram images (except random forest & CNN = best classifiers) [[*table of pairwise Welch's t-tests*](tables/results/ID1_pairwise_Wttest_inputs_comp.csv)]  
![mfccs vs. specs](pictures/results/mfcc_spec.png)  
*(left: McNemar's test MFCCs vs. Spec Img.s p-values for each cross-validation fold)*
- No differences in metrics scores when all combined
![mfccs vs.specs all](pictures/results/mfcc_spec_comparison_combined.png)  
- Large differences in processing times
![times](pictures/results/times_all.png)  
*[Welch’s ANOVA (F(9, 37.55)=9440.021, p<0.001, partial η2=0.999, N=91)*]
- Large differences in processing times between data types
![times mfccs vs. specs](pictures/results/mfcc_spec_runtimes.png)  

## 3.2 ID2
Differences between training context: SC outperforms DI & MIXED training on SC context test and performs = on different context test (DI or MIXED) [*table of average metric scores*](tables/results/ID2_avg_metrics.csv)  
![context_test](pictures/results/ID2_pairwise_comparison.png)  
[*pairwise Welch's ANOVA*](tables/results/ID2_pairwise_ANOVA.csv)

## 3.3 ID3
No difference between age category of training data [*table of average metric scores*](tables/results/ID3_avg_metrics.csv)  
![age_cat_test](pictures/results/ID3_comparison.png)  
[*pairwise Welch's ANOVA*](tables/results/ID3_pairwise_ANOVA.csv)

# Acknowledgements
I would like to express my immense gratitude towards my three supervisors Prof. Dr. Marta Manser, Dr. Ariana Strandburg-Peshkin, and Britta Walkenhorst for their continued support throughout this year. It was a pleasure to work with them and receive their many suggestions. I have particularly enjoyed the interdisciplinary nature of their supervision and could not have asked for better supervisors.  
I would also like to thank the Communication and Cognition in Social Mammals Group of the Department of Evolutionary Biology and Environmental studies at the University of Zurich.  
A special thanks goes also to all the past and present managers and volunteers of the Kalahari Meerkat Project (KMP) because I would have not been able to conduct such a project without the enormous amount of focal recordings data gathered throughout all these years.