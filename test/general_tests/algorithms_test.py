import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.layers import Dense, Conv2D, MaxPool2D, Flatten
from tensorflow.keras.regularizers import l1_l2, L2
from tensorflow.keras.optimizers import *
from tensorflow.keras.callbacks import EarlyStopping, LearningRateScheduler
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.dummy import DummyClassifier
from sklearn.cluster import KMeans
import numpy as np
import math
from ann_visualizer.visualize import ann_viz
import pickle


# Artificial Neural Network
class MLP:
    """
    1-hidden layer Multi-Layer Perceptron class.
    """

    def __init__(self, n_classes, random_state) -> None:
        """
        Model initialization.

        Args:
            n_classes [int]: number of target classes.
            random_state [int]: seed for reproducibility.

        Returns:
            None
        """

        tf.random.set_seed(random_state)

        l1_l2reg = l1_l2(l1=1e-2, l2=1e-2)

        self.model = keras.models.Sequential()

        self.model.add(Dense(
            units=675, activation="sigmoid"
        ))
        self.model.add(Dense(
            units=128, activation="sigmoid",
            kernel_regularizer=l1_l2reg
        ))
        self.model.add(Dense(
            units=n_classes, activation="softmax"
        ))

        self.model.compile(
            optimizer=Adam(learning_rate=1e-3),
            loss="categorical_crossentropy",
            metrics=["accuracy"]
        )

    def return_model(self):
        return self.model

    def print_summary(self):
        """
        Print model summary
        """

        print(self.model.summary())

    def save(self, path):
        """
        Save model

        Args:
            path [str]: file path
        """

        self.model.save(path)

    def load(self, path):
        """
        Load model

        Args:
            path [str]: file path
        """
        self.model = keras.models.load_model(path)

    def visualize(self, file_path, title=""):
        """
        Visualize model

        Args:
            file_path [str]: where to save the file and whith what name (.gv)
        """
        ann_viz(self.model, filename=file_path, title=title)

    def fit(self, X, y, batch_size=32, epochs=100):
        """
        Train the model.

        Args:
            X [ndarray]: training features
            y [ndarray]: training targets (one-hot encoded)
            batch_size [int]: default=32; training batch size
            epochs [int]: default=100; number of training epochs

        Returns:
            Training history
        """

        def learning_schedule(epoch):
            k = 1e-3
            return 1e-3 * math.exp(-k*epoch)

        lr_schedule = LearningRateScheduler(learning_schedule, verbose=0)

        early_stop = EarlyStopping(
            monitor="val_loss", patience=40, mode="min")

        history = self.model.fit(
            x=X, y=y,
            batch_size=batch_size, epochs=epochs, verbose=2,
            validation_split=0.2, callbacks=[lr_schedule, early_stop]
        )

        return history

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        pred_probs = self.model.predict(X)
        predicted = np.array([np.argmax(x) for x in self.model.predict(X)])

        return predicted, pred_probs


# Artificial Neural Network
class MLPspecs:
    """
    1-hidden layer Multi-Layer Perceptron class.
    """

    def __init__(self, n_classes, random_state) -> None:
        """
        Model initialization.

        Args:
            n_classes [int]: number of target classes.
            random_state [int]: seed for reproducibility.

        Returns:
            None
        """

        tf.random.set_seed(random_state)

        l1_l2reg = l1_l2(l1=1e-2, l2=1e-2)

        self.model = keras.models.Sequential()

        self.model.add(Dense(
            units=669, activation="relu",
            kernel_regularizer=l1_l2reg
        ))
        self.model.add(Dense(
            units=128, activation="relu",
            kernel_regularizer=l1_l2reg
        ))
        self.model.add(Dense(
            units=n_classes, activation="softmax"
        ))

        self.model.compile(
            optimizer=Adam(learning_rate=1e-5),
            loss="categorical_crossentropy",
            metrics=["accuracy"]
        )

    def return_model(self):
        return self.model

    def print_summary(self):
        """
        Print model summary
        """

        print(self.model.summary())

    def save(self, path):
        """
        Save model

        Args:
            path [str]: file path
        """

        self.model.save(path)

    def load(self, path):
        """
        Load model

        Args:
            path [str]: file path
        """
        self.model = keras.models.load_model(path)

    def visualize(self, file_path, title=""):
        """
        Visualize model

        Args:
            file_path [str]: where to save the file and whith what name (.gv)
        """
        ann_viz(self.model, filename=file_path, title=title)

    def fit(self, X, y, batch_size=32, epochs=100):
        """
        Train the model.

        Args:
            X [ndarray]: training features
            y [ndarray]: training targets (one-hot encoded)
            batch_size [int]: default=32; training batch size
            epochs [int]: default=100; number of training epochs

        Returns:
            Training history
        """

        def learning_schedule(epoch):
            k = 1e-5
            return 1e-5 * math.exp(-k*epoch)

        lr_schedule = LearningRateScheduler(learning_schedule, verbose=0)

        early_stop = EarlyStopping(
            monitor="val_loss", patience=40, mode="min")

        history = self.model.fit(
            x=X, y=y,
            batch_size=batch_size, epochs=epochs, verbose=2,
            validation_split=0.2, callbacks=[lr_schedule, early_stop]
        )

        return history

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        pred_probs = self.model.predict(X)
        predicted = np.array([np.argmax(x) for x in self.model.predict(X)])

        return predicted, pred_probs


# Convolutional Neural Network
class CNN:
    """
    Convolutional Neural Network class: 
        - 3 Conv2D layers
        - 3 MaxPool2D layers
        - Flatten layer
        - 1 hidden Dense layer
    """

    def __init__(self, input_shape, n_classes, random_state) -> None:
        """
        Model initialization.

        Args:
            input_shape [tuple]: input array's shape
            n_classes [int]: number of target classes.
            random_state [int]: seed for reproducibility.

        Returns:
            None
        """

        tf.random.set_seed(random_state)

        l2_reg = L2(l2=1e-2)

        self.model = keras.Sequential()

        self.model.add(Conv2D(
            filters=512, kernel_size=(3, 3),
            activation="relu", input_shape=input_shape
        ))
        self.model.add(MaxPool2D((2, 2)))
        self.model.add(Conv2D(
            filters=352, kernel_size=(3, 3),
            activation="relu",
            activity_regularizer=l2_reg
        ))
        self.model.add(MaxPool2D((2, 2))),
        self.model.add(Conv2D(
            filters=256, kernel_size=(3, 3),
            activation="relu",
            activity_regularizer=l2_reg
        ))
        self.model.add(MaxPool2D((2, 2)))
        self.model.add(Flatten())
        self.model.add(Dense(
            units=32, activation="relu",
            activity_regularizer=l2_reg
        ))
        self.model.add(Dense(
            units=n_classes, activation="softmax"
        ))

        self.model.compile(
            optimizer=Adam(learning_rate=1e-4),
            loss="categorical_crossentropy",
            metrics=["accuracy"]
        )

    def return_model(self):
        return self.model

    def print_summary(self):
        """
        Print model summary
        """

        print(self.model.summary())

    def save(self, path):
        """
        Save model

        Args:
            path [str]: file path
        """
        self.model.save(path)

    def load(self, path):
        """
        Load model

        Args:
            path [str]: file path
        """

        self.model = keras.models.load_model(path)

    def fit(self, X, y, batch_size=16, epochs=70):
        """
        Train the model.

        Args:
            X [ndarray]: training arrays
            y [ndarray]: training targets (one-hot encoded)
            batch_size [int]: default=16; training batch size
            epochs [int]: default=70; number of training epochs

        Returns:
            Training history
        """

        # def learning_schedule(epoch):
        #     k =1e-4
        #     return 1e-4 * math.exp(-k*epoch)

        # lr_schedule = LearningRateScheduler(learning_schedule, verbose=0)

        early_stop = EarlyStopping(monitor="val_loss", patience=10, mode="min")

        history = self.model.fit(
            x=X, y=y,
            batch_size=batch_size, epochs=epochs,
            callbacks=[early_stop], verbose=2,
            validation_split=0.2
        )

        return history

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test arrays

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        pred_probs = self.model.predict(X)
        predicted = np.array([np.argmax(x) for x in self.model.predict(X)])

        return predicted, pred_probs


# Random Forests
class RandForest:
    """
    Random Forest classifier class
    """

    def __init__(self, random_state) -> None:
        """
        Model initialization.

        Args:
            random_state [int]: seed for reproducibility.

        Returns:
            None
        """

        self.clf = RandomForestClassifier(
            n_estimators=512, min_samples_split=20,
            max_leaf_nodes=640, max_features="log2",
            max_depth=128, criterion="gini",
            random_state=random_state, n_jobs=-1
        )

    def return_model(self):
        return self.model

    # def print_summary(self):
    #     """
    #     Print model summary
    #     """

    def save(self, path):
        """
        Save model

        Args:
            path [str]: üath to file (.sav)
        """

        with open(path, "wb") as fb:
            pickle(self.model, fb)

    def load(self, path):
        """
        Save model

        Args:
            path [str]: üath to file (.sav)
        """

        with open(path, "rb") as fb:
            self.model = pickle.load(fb)

    def fit(self, X, y):
        """
        Train the model.

        Args:
            X [ndarray]: training features
            y [ndarray]: training targets

        Returns:
            None
        """

        self.clf.fit(X=X, y=y)

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        predicted = self.clf.predict(X)
        pred_probs = self.clf.predict_proba(X)

        return predicted, pred_probs


class RandForestDimRed:
    """
    Random Forest classifier class
    """

    def __init__(self, random_state) -> None:
        """
        Model initialization.

        Args:
            random_state [int]: seed for reproducibility.

        Returns:
            None
        """

        self.clf = RandomForestClassifier(
            random_state=random_state, n_jobs=-1
        )

    def return_model(self):
        return self.model

    # def print_summary(self):
    #     """
    #     Print model summary
    #     """

    def save(self, path):
        """
        Save model

        Args:
            path [str]: üath to file (.sav)
        """

        with open(path, "wb") as fb:
            pickle(self.model, fb)

    def load(self, path):
        """
        Save model

        Args:
            path [str]: üath to file (.sav)
        """

        with open(path, "rb") as fb:
            self.model = pickle.load(fb)

    def fit(self, X, y):
        """
        Train the model.

        Args:
            X [ndarray]: training features
            y [ndarray]: training targets

        Returns:
            None
        """

        self.clf.fit(X=X, y=y)

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        predicted = self.clf.predict(X)
        pred_probs = self.clf.predict_proba(X)

        return predicted, pred_probs


# Multinomial Regression
class MultinomReg:
    """
    Multinomial Regression classifier class
    """

    def __init__(self, random_state) -> None:
        """
        Model initialization.

        Args:
            random_state [int]: seed for reproducibility.

        Returns:
            None
        """

        self.model = LogisticRegression(
            multi_class="multinomial", solver="lbfgs",
            penalty="l2", C=0.9, verbose=0, random_state=random_state, max_iter=10000
        )

    def save(self, path):
        """
        Save model

        Args:
            path [str]: üath to file (.sav)
        """

        with open(path, "wb") as fb:
            pickle(self.model, fb)

    def load(self, path):
        """
        Save model

        Args:
            path [str]: üath to file (.sav)
        """

        with open(path, "rb") as fb:
            self.model = pickle.load(fb)

    def fit(self, X, y):
        """
        Train the model.

        Args:
            X [ndarray]: training features
            y [ndarray]: training targets

        Returns:
            None
        """

        self.model.fit(X, y)

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        predicted = self.model.predict(X)

        pred_probs = self.model.predict_proba(X)

        return predicted, pred_probs


# K-Means
class Kmeans:
    """
    K-means classifier
    """

    def __init__(self, n_classes, random_state) -> None:
        """
        Model initialization.

        Args:
            n_classes [int]: number of target classes.
            random_state [int]: seed for reproducibility.

        Returns:
            None
        """

        self.clf = KMeans(
            n_clusters=n_classes, init="k-means++", n_init=50,
            max_iter=500, random_state=random_state, algorithm="full"
        )

    def save(self, path):
        """
        Save model

        Args:
            path [str]: üath to file (.sav)
        """

        with open(path, "wb") as fb:
            pickle(self.model, fb)

    def load(self, path):
        """
        Save model

        Args:
            path [str]: üath to file (.sav)
        """

        with open(path, "rb") as fb:
            self.model = pickle.load(fb)

    def fit(self, X):
        """
        Train the model.

        Args:
            X [ndarray]: training features

        Returns:
            None
        """

        self.clf.fit(X)

    def predict(self, X, y_test):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features
            y_test [ndarray]: true test targets (used for most-likely cluster classification)

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        k_labels = self.clf.predict(X)

        predicted = np.empty_like(k_labels)
        pred_probs = np.empty(
            shape=(len(k_labels), len(np.unique(y_test))),
            dtype=float
        )

        for k in np.unique(k_labels):
            match = [np.sum((k_labels == k)*(y_test == t))
                     for t in np.unique(y_test)]
            predicted[k_labels == k] = np.unique(y_test)[
                np.argmax(match)]
            pred_probs[k_labels == k] = match/np.sum(match)

        return predicted, pred_probs


# Dummy Classifier
class Dummy:
    """
    Baseline Dummy classifier (stratified strategy)
    """

    def __init__(self, random_state) -> None:
        """
        Model initialization.

        Args:
            random_state [int]: seed for reproducibility.

        Returns:
            None
        """

        self.model = DummyClassifier(
            strategy="stratified",
            random_state=random_state
        )

    def save(self, path):
        """
        Save model

        Args:
            path [str]: üath to file (.sav)
        """

        with open(path, "wb") as fb:
            pickle(self.model, fb)

    def load(self, path):
        """
        Save model

        Args:
            path [str]: üath to file (.sav)
        """

        with open(path, "rb") as fb:
            self.model = pickle.load(fb)

    def fit(self, X, y):
        """
        Train the model.

        Args:
            X [ndarray]: training features
            y [ndarray]: training targets

        Returns:
            None
        """

        self.model.fit(X=X, y=y)

    def predict(self, X):
        """
        Predict using the trained model.

        Args:
            X [ndarray]: test features

        Returns:
            predicted [ndarray] = class predictions
            pred_probs [ndarray] = predicted probabilities of each class
        """

        predicted = self.model.predict(X=X)

        pred_probs = self.model.predict_proba(X=X)

        return predicted, pred_probs
