# %%
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
import numpy as np

# %%
with open("C:/Users/alessandro/Documents/UZH/Thesis/code/general_identification/by_date/results/rf/metrics.p", "rb") as f:
    metr = pickle.load(f)
metr

# %%
tab = metr["table"]
tab

# %%
tab.drop(20, inplace=True)
tab

# %%
tab["Sum"] = tab["Precision"] + tab["Recall"]

# %%
meta = pd.read_csv("metadata.csv")
meta.info()

# %%
shared_metadata = pd.read_excel("SoundFilesData.xlsx")
shared_metadata.info()

# %%
meta["RecDate"] = pd.to_datetime(meta["RecDate"], format="%Y-%m-%d")

# %%
shared_metadata["DateRec"] = pd.to_datetime(
    shared_metadata["DateRec"], format="%m/%d/%Y", errors="coerce")

# %%
shared_metadata.drop(shared_metadata[~shared_metadata["IndividID"].isin(
    meta["ID"].unique())].index, inplace=True)

# %%
training = pd.read_csv("training.csv")
training.info()

# %%
tr_size = training.groupby(["Code"]).size().to_frame("N").reset_index()
tr_size


# %%
res_and_train = pd.concat([tab, tr_size], axis=1)
res_and_train

# %%
# %%
plotting = res_and_train.sort_values(by="N")
plt.figure(figsize=(5, 5))
plotting.plot("N", "Sum")
plt.show()

# %%
plotting = pd.merge(
    plotting, meta[["Code", "GroupName"]], on="Code", how="inner")

# %%
plotting.boxplot(column="Sum", by="GroupName")

# %%
