import pandas as pd
import numpy as np
import librosa
import librosa.display
import soundfile
import glob
import matplotlib
import matplotlib.image
import matplotlib.pyplot as plt


class augment_audio:

    def __init__(self,  directory) -> None:
        """
        Initialization of augmenter for signals using noise.

        Args:
            directory [str]: directory of noise samples
        """
        filenames = glob.glob(directory+"*.wav")

        self.noise = []

        for i in filenames:
            signal, _ = librosa.load(i, sr=None)

            self.noise.append(signal)

    def Augment(self, signals, sample_rates, metadata, directory, N, seed=42, aug_type="both"):
        """
        Adds random amount of noise or shifts (or both) a signal creating `how_many` augmented signals.
        Then also saves this signals in the augmented/ directory.

        Args:
            signals: [list] list of signals from librosa.load
            sample_rates: [list] list of sample rate of each signal
            metadata: [pd.DataFrame] metadata dataframe
            directory: [str] directory where to save the augmented files
            N: [int] how many signals
            seed [int]: default=42; random seed for reproducibility
            aug_type [str]: default="both"; type of augmentation:
                            noise(adds only noise), shift (only shifts), both (default), random (random choice between the other options for each augmentation)

        Returns:
            df: [pd.DataFrane] updated metadata dataframe with augmented file information
        """

        if not(aug_type in ["noise", "shift", "both", "random"]):
            raise ValueError(
                "aug_type value does not correspond to any of the available options")

        if aug_type == "random":
            is_random = True
        else:
            is_random = False

        df = metadata.copy(deep=True)

        np.random.seed(seed)

        n_calls = metadata.shape[0]

        # how many augmented signals per original call
        how_many = (N-n_calls)//n_calls

        j = 0  # call counter

        for i, sig in enumerate(signals):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]
            n = 1  # sequential numbering of augmented signals

            j += 1  # keep count also of original signals

            # random selection of noise
            selection = [np.random.randint(
                0, len(self.noise) - 1) for j in range(how_many)]

            for k in selection:

                if is_random:
                    aug_type = np.random.choice(["noise", "shift", "both"])

                if aug_type == "noise":
                    # resize noise to siganl dims
                    tmp_noise = np.resize(self.noise[k], sig.shape)

                    # augment signal
                    augmented = sig + tmp_noise

                    # metadata rewrite
                    tmp_series["CallFile"] = call_file + \
                        "_aug_{:02}".format(n) + ".WAV"
                    new_path = directory + "\\" + call_file + \
                        "_aug_{:02}".format(n) + ".WAV"
                    tmp_series["Path"] = new_path
                    n += 1

                    df = df.append(tmp_series)

                    # save augmented file
                    soundfile.write(
                        file=new_path, data=augmented, samplerate=sr
                    )

                    j += 1

                if aug_type == "shift":

                    # get random shift:
                    s = int(np.random.choice(
                        [-1, 1]) * np.random.uniform(0.1, 0.3) * len(sig))

                    augmented = np.roll(sig, s)

                    # padding
                    if s > 0:
                        augmented[:s] = 0
                    else:
                        augmented[s:] = 0

                    # metadata rewrite
                    tmp_series["CallFile"] = call_file + \
                        "_aug_{:02}".format(n) + ".WAV"
                    new_path = directory + "\\" + call_file + \
                        "_aug_{:02}".format(n) + ".WAV"
                    tmp_series["Path"] = new_path
                    n += 1

                    df = df.append(tmp_series)

                    # save augmented file
                    soundfile.write(
                        file=new_path, data=augmented, samplerate=sr)

                    j += 1

                if aug_type == "both":

                    # resize noise to siganl dims
                    tmp_noise = np.resize(self.noise[k], sig.shape)

                    # augment signal
                    augmented = sig + tmp_noise

                    # get random shift:
                    s = int(np.random.choice(
                        [-1, 1]) * np.random.uniform(0.1, 0.3) * len(sig))

                    augmented = np.roll(augmented, s)

                    # padding
                    if s > 0:
                        augmented[:s] = 0
                    else:
                        augmented[s:] = 0

                    # metadata rewrite
                    tmp_series["CallFile"] = call_file + \
                        "_aug_{:02}".format(n) + ".WAV"
                    new_path = directory + "\\" + call_file + \
                        "_aug_{:02}".format(n) + ".WAV"
                    tmp_series["Path"] = new_path
                    n += 1

                    df = df.append(tmp_series)

                    # save augmented file
                    soundfile.write(
                        file=new_path, data=augmented, samplerate=sr)

                    j += 1

        d = {}  # dictionary for correct sequential numbering of augmented calls

        # augment random signals again to reach N
        while j < N:

            # random original call
            i = np.random.randint(0, metadata.shape[0] - 1)

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sig = signals[i]  # signal
            sr = sample_rates[i]  # sampling rate of the signal

            # sequential numbering
            if not(call_file in d.keys()):
                n = how_many + 1
                d["call_file"] = [n]
            else:
                n = how_many + 1 + len(d["call_file"])
                d["call_file"].append(n)

            # select random noise sample
            tmp_noise = self.noise[np.random.randint(0, len(self.noise)-1)]

            # resize noise to siganl dims
            tmp_noise = np.resize(tmp_noise, sig.shape)

            if is_random:
                aug_type = np.random.choice(["noise", "shift", "both"])

            if aug_type == "noise":

                # augment signal
                augmented = sig + tmp_noise

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr
                )

                j += 1

            if aug_type == "shift":

                # get random shift:
                s = int(np.random.choice(
                    [-1, 1]) * np.random.uniform(0.1, 0.3) * len(sig))

                augmented = np.roll(sig, s)

                # padding
                if s > 0:
                    augmented[:s] = 0
                else:
                    augmented[s:] = 0

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr)

                j += 1

            if aug_type == "both":

                # augment signal
                augmented = sig + tmp_noise

                # get random shift:
                s = int(np.random.choice(
                    [-1, 1]) * np.random.uniform(0.1, 0.3) * len(sig))

                augmented = np.roll(sig, s)

                # padding
                if s > 0:
                    augmented[:s] = 0
                else:
                    augmented[s:] = 0

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr)

                j += 1

    def SigAugment(self, signals, sample_rates, metadata, directory, N, seed=42):
        """
        Adds random amount of noise to a signal creating `how_many` augmented signals to reach N.
        Then also saves this signals in the augmented/ directory.

        Args:
            signals: [list] list of signals from librosa.load
            sample_rates: [list] list of sample rate of each signal
            metadata: [pd.DataFrame] metadata dataframe
            directory: [str] directory where to save the augmented files
            N: [int] how many signals
            seed [int]: default=42; random seed for reproducibility
            remain: [float] default=0.0; if there is a remainder

        Returns:
            df: [pd.DataFrane] updated metadata dataframe with augmented file information
        """

        df = metadata.copy(deep=True)

        np.random.seed(seed)

        n_calls = metadata.shape[0]

        # how many augmented signals per original call
        how_many = (N-n_calls)//n_calls

        j = 0  # call counter

        for i, sig in enumerate(signals):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]  # sampling rate of the signal
            n = 1  # sequential numbering of augmented signals

            j += 1  # keep count also of original signals

            selection = [np.random.randint(
                0, len(self.noise) - 1) for j in range(how_many)]

            # noise augmentation
            for k in selection:

                # resize noise to siganl dims
                tmp_noise = np.resize(self.noise[k], sig.shape)

                # augment signal
                augmented = sig + tmp_noise

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr
                )

                j += 1

        d = {}  # dictionary for correct sequential numbering of augmented calls

        # augment random signals again to reach N
        while j < N:

            # random original call
            i = np.random.randint(0, metadata.shape[0] - 1)
            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sig = signals[i]  # signal
            sr = sample_rates[i]  # sampling rate of the signal

            # sequential numbering
            if not(call_file in d.keys()):
                n = how_many + 1
                d["call_file"] = [n]
            else:
                n = how_many + 1 + len(d["call_file"])
                d["call_file"].append(n)

            # select random noise sample
            tmp_noise = self.noise[np.random.randint(0, len(self.noise)-1)]

            # resize noise to siganl dims
            tmp_noise = np.resize(tmp_noise, sig.shape)

            # augment signal
            augmented = sig + tmp_noise

            # metadata rewrite
            tmp_series["CallFile"] = call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            new_path = directory + "\\" + call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            tmp_series["Path"] = new_path
            n += 1

            df = df.append(tmp_series)

            # save augmented file
            soundfile.write(
                file=new_path, data=augmented, samplerate=sr)

            j += 1

        return df

    def ShiftAugment(self, signals, sample_rates, metadata, directory, N, seed=42):
        """
        Shifts signal creating `how_many` augmented signals.
        Then also saves this signals in the augmented/ directory.

        Args:
            signals: [list] list of signals from librosa.load
            sample_rates: [list] list of sample rate of each signal
            metadata: [pd.DataFrame] metadata dataframe
            directory: [str] directory where to save the augmented files
            N: [int] how many signals
            seed [int]: default=42; random seed for reproducibility
            remain: [float] default=0.0; if there is a remainder

        Returns:
            df: [pd.DataFrane] updated metadata dataframe with augmented file information
        """

        df = metadata.copy(deep=True)

        np.random.seed(seed)

        n_calls = metadata.shape[0]

        # how many augmented signals per original call
        how_many = (N-n_calls)//n_calls

        j = 0  # call counter

        for i, sig in enumerate(signals):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]  # sampling rate of the signal
            n = 1  # sequential numbering of augmented signals

            j += 1  # keep count also of original signals

            # shift augmentation
            for k in range(how_many):

                # get random shift:
                s = int(np.random.choice(
                    [-1, 1]) * np.random.uniform(0.1, 0.3) * len(sig))

                augmented = np.roll(sig, s)

                # padding
                if s > 0:
                    augmented[:s] = 0
                else:
                    augmented[s:] = 0

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr)

                j += 1

        d = {}  # dictionary for correct sequential numbering of augmented calls

        # augment random signals again to reach N
        while j < N:

            # random original call
            i = np.random.randint(0, metadata.shape[0] - 1)

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sig = signals[i]  # signal
            sr = sample_rates[i]  # sampling rate of the signal

            # sequential numbering
            if not(call_file in d.keys()):
                n = how_many + 1
                d["call_file"] = [n]
            else:
                n = how_many + 1 + len(d["call_file"])
                d["call_file"].append(n)

            # get random shift:
            s = int(np.random.choice(
                [-1, 1]) * np.random.uniform(0.1, 0.3) * len(sig))

            augmented = np.roll(sig, s)

            # padding
            if s > 0:
                augmented[:s] = 0
            else:
                augmented[s:] = 0

            # metadata rewrite
            tmp_series["CallFile"] = call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            new_path = directory + "\\" + call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            tmp_series["Path"] = new_path
            n += 1

            df = df.append(tmp_series)

            # save augmented file
            soundfile.write(
                file=new_path, data=augmented, samplerate=sr)

            j += 1

        return df

    def PitchAugmentLibr(self, min_shift_Hz, max_shift_Hz, signals, sample_rates, metadata, directory, N, seed=42):
        """
        Pitch shifting using librosa.effects.pitch_shift creating `how_many` augmented signals.
        Then also saves this signals in the augmented/ directory.

        Args:
            min_shift_Hz: [float] minimum amount to shift in Hz
            max_shift_Hz: [float] maximum amount to shift in Hz 
            signals: [list] list of signals from librosa.load
            sample_rates: [list] list of sample rate of each signal
            metadata: [pd.DataFrame] metadata dataframe
            directory: [str] directory where to save the augmented files
            N: [int] how many signals
            seed [int]: default=42; random seed for reproducibility
            remain: [float] default=0.0; if there is a remainder

        Formula: f1 = f0*2^(s/12) (s=num. steps)
                 s = 12 log_2(1 + F_shift/f0)
                 f0 = i/N * sr (i=pos.max.magn, N=num.samples)

        Returns:
            df: [pd.DataFrane] updated metadata dataframe with augmented file information
        """

        df = metadata.copy(deep=True)

        np.random.seed(seed)

        n_calls = metadata.shape[0]

        # how many augmented signals per original call
        how_many = (N-n_calls)//n_calls

        j = 0  # call counter

        for i, sig in enumerate(signals):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]  # sampling rate of the signal
            n = 1  # sequential numbering of augmented signals

            j += 1  # keep count also of original signals

            sig_fft = np.fft.rfft(sig)
            f0 = np.where(sig_fft == np.max(sig_fft))
            f0 = f0[0] / len(sig) * sr
            # avoid 0 division later
            if not(f0 > 0.0):
                f0 = f0 + 0.3

            for k in range(how_many):

                up_down = np.random.choice([-1, 1])  # shift up or down
                shift_Hz = np.random.uniform(min_shift_Hz, max_shift_Hz)
                s = up_down * 12 * np.log2(1 + shift_Hz/f0)

                augmented = librosa.effects.pitch_shift(
                    y=sig, sr=sr, n_steps=s)

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr)

                j += 1

        d = {}  # dictionary for correct sequential numbering of augmented calls

        # augment random signals again to reach N
        while j < N:

            # random original call
            i = np.random.randint(0, metadata.shape[0] - 1)
            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sig = signals[i]  # signal
            sr = sample_rates[i]  # sampling rate of the signal

            # sequential numbering
            if not(call_file in d.keys()):
                n = how_many + 1
                d["call_file"] = [n]
            else:
                n = how_many + 1 + len(d["call_file"])
                d["call_file"].append(n)

            sig_fft = np.fft.rfft(sig)  # signal fft
            f0 = np.where(sig_fft == np.max(sig_fft))
            f0 = f0[0] / len(sig) * sr
            # avoid 0 division later
            if not(f0 > 0.0):
                f0 = f0 + 0.3

            up_down = np.random.choice([-1, 1])  # shift up or down
            shift_Hz = np.random.uniform(min_shift_Hz, max_shift_Hz)
            s = up_down * 12 * np.log2(1 + shift_Hz/f0)

            augmented = librosa.effects.pitch_shift(
                y=sig, sr=sr, n_steps=s)

            # metadata rewrite
            tmp_series["CallFile"] = call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            new_path = directory + "\\" + call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            tmp_series["Path"] = new_path
            n += 1

            df = df.append(tmp_series)

            # save augmented file
            soundfile.write(
                file=new_path, data=augmented, samplerate=sr)

            j += 1

        return df

    def PitchAugmentNp(self, min_shift_Hz, max_shift_Hz, signals, sample_rates, metadata, directory, N, seed=42):
        """
        Pitch shifting using numpy creating `how_many` augmented signals.
        Then also saves this signals in the augmented/ directory.

        Args:
            min_shift_Hz: [float] minimum amount to shift in Hz
            max_shift_Hz: [float] maximum amount to shift in Hz 
            signals: [list] list of signals from librosa.load
            sample_rates: [list] list of sample rate of each signal
            metadata: [pd.DataFrame] metadata dataframe
            directory: [str] directory where to save the augmented files
            N: [int] how many signals
            seed [int]: default=42; random seed for reproducibility
            remain: [float] default=0.0; if there is a remainder

        Formula: F_shift:sr=shift:N (N=num. samples)
                 shift = round(F_shift * N / sr)

        Returns:
            df: [pd.DataFrane] updated metadata dataframe with augmented file information
        """

        df = metadata.copy(deep=True)

        np.random.seed(seed)

        n_calls = metadata.shape[0]

        # how many augmented signals per original call
        how_many = (N-n_calls)//n_calls

        j = 0  # call counter

        for i, sig in enumerate(signals):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]  # sampling rate of the signal
            n = 1  # sequential numbering of augmented signals

            j += 1  # keep count also of original signals

            sig_fft = np.fft.rfft(sig)  # fft of signal

            for k in range(how_many):

                shift_Hz = np.random.choice(
                    [-1, 1]) * np.random.uniform(min_shift_Hz, max_shift_Hz)

                s = int(np.round(shift_Hz * len(sig) / sr))
                augmented = np.roll(sig_fft, s)
                augmented = np.fft.irfft(augmented)

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr)

                j += 1

        d = {}  # dictionary for correct sequential numbering of augmented calls

        # augment random signals again to reach N
        while j < N:

            # random original call
            i = np.random.randint(0, metadata.shape[0] - 1)
            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sig = signals[i]  # signal
            sr = sample_rates[i]  # sampling rate of the signal

            # sequential numbering
            if not(call_file in d.keys()):
                n = how_many + 1
                d["call_file"] = [n]
            else:
                n = how_many + 1 + len(d["call_file"])
                d["call_file"].append(n)

            sig_fft = np.fft.rfft(sig)  # fft of signal

            shift_Hz = np.random.choice(
                [-1, 1]) * np.random.uniform(min_shift_Hz, max_shift_Hz)

            s = int(np.round(shift_Hz * len(sig) / sr))
            augmented = np.roll(sig_fft, s)
            augmented = np.fft.irfft(augmented)

            # metadata rewrite
            tmp_series["CallFile"] = call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            new_path = directory + "\\" + call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            tmp_series["Path"] = new_path
            n += 1

            df = df.append(tmp_series)

            # save augmented file
            soundfile.write(
                file=new_path, data=augmented, samplerate=sr)

            j += 1

        return df

    def HarmAugment(self, signals, sample_rates, metadata, directory, N, seed=42):
        """
        Harmonic distortion creating `how_many` augmented signals.
        Then also saves this signals in the augmented/ directory.

        Args:
            signals: [list] list of signals from librosa.load
            sample_rates: [list] list of sample rate of each signal
            metadata: [pd.DataFrame] metadata dataframe
            directory: [str] directory where to save the augmented files
            N: [int] how many signals
            seed [int]: default=42; random seed for reproducibility
            remain: [float] default=0.0; if there is a remainder

        Formula: sig_out = amount * sin(2*pi*sig_in)

        Returns:
            df: [pd.DataFrane] updated metadata dataframe with augmented file information
        """

        df = metadata.copy(deep=True)

        np.random.seed(seed)

        n_calls = metadata.shape[0]

        # how many augmented signals per original call
        how_many = (N-n_calls)//n_calls

        j = 0  # call counter

        for i, sig in enumerate(signals):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]  # sampling rate of the signal
            n = 1  # sequential numbering of augmented signals

            j += 1  # keep count also of original signals

            for k in range(how_many):

                # how much harmonic distortion to keep
                amount = np.random.uniform(0.6, 1.0)
                augmented = amount * np.sin(2 * np.pi * sig)

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr)

                j += 1

        d = {}  # dictionary for correct sequential numbering of augmented calls

        # augment random signals again to reach N
        while j < N:

            # random original call
            i = np.random.randint(0, metadata.shape[0] - 1)
            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sig = signals[i]  # signal
            sr = sample_rates[i]  # sampling rate of the signal

            # sequential numbering
            if not(call_file in d.keys()):
                n = how_many + 1
                d["call_file"] = [n]
            else:
                n = how_many + 1 + len(d["call_file"])
                d["call_file"].append(n)

            # how much harmonic distortion to keep
            amount = np.random.uniform(0.6, 1.0)
            augmented = amount * np.sin(2 * np.pi * sig)

            # metadata rewrite
            tmp_series["CallFile"] = call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            new_path = directory + "\\" + call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            tmp_series["Path"] = new_path
            n += 1

            df = df.append(tmp_series)

            # save augmented file
            soundfile.write(
                file=new_path, data=augmented, samplerate=sr)

            j += 1

        return df

    def shift_f0(self, min_shift_Hz, max_shift_Hz, signals, sample_rates, metadata, directory, N, seed=42):
        """
        Shifting fundamental frequency using numpy creating `how_many` augmented signals.
        Then also saves this signals in the augmented/ directory.

        Args:
            min_shift_Hz: [float] minimum amount to shift in Hz
            max_shift_Hz: [float] maximum amount to shift in Hz
            signals: [list] list of signals from librosa.load
            sample_rates: [list] list of sample rate of each signal
            metadata: [pd.DataFrame] metadata dataframe
            directory: [str] directory where to save the augmented files
            N: [int] how many signals
            seed [int]: default=42; random seed for reproducibility
            remain: [float] default=0.0; if there is a remainder

        Formula: F_shift:sr=shift:N (N=num. samples)
                 shift = round(F_shift * N / sr)

        Returns:
            df: [pd.DataFrane] updated metadata dataframe with augmented file information
        """

        df = metadata.copy(deep=True)

        np.random.seed(seed)

        n_calls = metadata.shape[0]

        # how many augmented signals per original call
        how_many = (N-n_calls)//n_calls

        j = 0  # call counter

        for i, sig in enumerate(signals):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]  # sampling rate of the signal
            n = 1  # sequential numbering of augmented signals

            j += 1  # keep count also of original signals

            sig_freq = np.fft.rfft(sig)  # sig fft
            sig_freq_abs = np.abs(sig_freq)  # absolute sig fft

            f0 = np.where(sig_freq_abs == np.max(
                sig_freq_abs))[0]  # fundamental

            start = (f0 - 30)[0]
            end = (f0 + 31)[0]

            if start < 0:
                start = 0
            if end > (len(sig_freq) - 1):
                end = len(sig_freq) - 1

            # noise for substitution (from the same signal)
            where_noise = np.where(
                (sig_freq_abs < np.max(sig_freq_abs) * 0.05))

            noise = np.random.choice(sig_freq[where_noise], size=len(
                sig_freq[start:end]))  # random noise

            for k in range(how_many):

                shift_Hz = np.random.choice(
                    [-1, 1]) * np.random.uniform(min_shift_Hz, max_shift_Hz)

                s = int(np.round(shift_Hz * len(sig) / sr))

                if (start + s) < 0:
                    s *= -1
                elif (end + s) > (len(sig_freq) - 1):
                    s *= -1

                augmented = sig_freq.copy()

                start_s = start + s
                end_s = end + s

                augmented[start:end] = noise
                augmented[start_s:end_s] = sig_freq[start:end]
                augmented = np.fft.irfft(augmented)

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr)

                j += 1

        d = {}  # dictionary for correct sequential numbering of augmented calls

        # augment random signals again to reach N
        while j < N:

            # random original call
            i = np.random.randint(0, metadata.shape[0] - 1)
            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sig = signals[i]  # signal
            sr = sample_rates[i]  # sampling rate of the signal

            # sequential numbering
            if not(call_file in d.keys()):
                n = how_many + 1
                d["call_file"] = [n]
            else:
                n = how_many + 1 + len(d["call_file"])
                d["call_file"].append(n)

            sig_freq = np.fft.rfft(sig)  # sig fft
            sig_freq_abs = np.abs(sig_freq)  # absolute sig fft

            f0 = np.where(sig_freq_abs == np.max(
                sig_freq_abs))[0]  # fundamental

            start = (f0 - 30)[0]
            end = (f0 + 31)[0]

            if start < 0:
                start = 0
            if end > (len(sig_freq) - 1):
                end = len(sig_freq) - 1

            # noise for substitution (from the same signal)
            where_noise = np.where(
                (sig_freq_abs < np.max(sig_freq_abs) * 0.05))

            noise = np.random.choice(sig_freq[where_noise], size=len(
                sig_freq[start:end]))  # random noise

            shift_Hz = np.random.choice(
                [-1, 1]) * np.random.uniform(min_shift_Hz, max_shift_Hz)

            s = int(np.round(shift_Hz * len(sig) / sr))

            if (start + s) < 0:
                s *= -1
            elif (end + s) > (len(sig_freq) - 1):
                s *= -1

            augmented = sig_freq.copy()

            start_s = start + s
            end_s = end + s

            augmented[start:end] = noise
            augmented[start_s:end_s] = sig_freq[start:end]
            augmented = np.fft.irfft(augmented)

            # metadata rewrite
            tmp_series["CallFile"] = call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            new_path = directory + "\\" + call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            tmp_series["Path"] = new_path
            n += 1

            df = df.append(tmp_series)

            # save augmented file
            soundfile.write(
                file=new_path, data=augmented, samplerate=sr)

            j += 1

        return df

    def shift_formants(self, min_shift_Hz, max_shift_Hz, signals, sample_rates, metadata, directory, N, seed=42, threshold=0.15):
        """
        Shifting higher harmonics using numpy creating `how_many` augmented signals.
        Then also saves this signals in the augmented/ directory.

        Args:
            min_shift_Hz: [float] minimum amount to shift in Hz
            max_shift_Hz: [float] maximum amount to shift in Hz
            signals: [list] list of signals from librosa.load
            sample_rates: [list] list of sample rate of each signal
            metadata: [pd.DataFrame] metadata dataframe
            directory: [str] directory where to save the augmented files
            N: [int] how many signals
            seed [int]: default=42; random seed for reproducibility
            threshold [float]: default=0.15; lowest amount in relation to f0 to count as harmonic and not noise

        Formula: F_shift:sr=shift:N (N=num. samples)
                 shift = round(F_shift * N / sr)

        Returns:
            df: [pd.DataFrane] updated metadata dataframe with augmented file information
        """

        if threshold > 1.0:
            raise Exception("quantile should not be >1.0")

        df = metadata.copy(deep=True)

        np.random.seed(seed)

        n_calls = metadata.shape[0]

        # how many augmented signals per original call
        how_many = (N-n_calls)//n_calls

        j = 0  # call counter

        for i, sig in enumerate(signals):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]  # sampling rate of the signal
            n = 1  # sequential numbering of augmented signals

            j += 1  # keep count also of original signals

            sig_freq = np.fft.rfft(sig)  # sig fft
            sig_freq_abs = np.abs(sig_freq)  # absolute sig fft

            formants = np.where((sig_freq_abs < np.max(sig_freq_abs))
                                * (sig_freq_abs > threshold * np.max(sig_freq_abs)))
            f0 = np.where(sig_freq_abs == np.max(
                sig_freq_abs))[0]  # fundamental

            # noise for substitution (from the same signal)
            where_noise = np.where(
                (sig_freq_abs < np.max(sig_freq_abs) * 0.05))

            for k in range(how_many):

                shift_Hz = np.random.choice(
                    [-1, 1]) * np.random.uniform(min_shift_Hz, max_shift_Hz)

                s = int(np.round(shift_Hz * len(sig) / sr))

                augmented = sig_freq.copy()

                for i in formants[0]:
                    # exclude close to f0
                    if i > (f0-30)[0] and i < (f0+31)[0]:
                        pass
                    else:
                        start = (i - 15)
                        end = (i + 16)
                        if start < 0:
                            start = 0
                        if end > (len(sig_freq) - 1):
                            end = len(sig_freq) - 1

                        if (start + s) < 0:
                            s *= -1
                        elif (end + s) > (len(sig_freq) - 1):
                            s *= -1

                        start_s = start + s
                        end_s = end + s

                        noise = np.random.choice(sig_freq[where_noise], size=len(
                            sig_freq[start:end]))  # random noise
                        augmented[start:end] = np.random.choice(
                            noise, size=end-start)
                        augmented[start_s:end_s] = sig_freq[start:end]

                augmented = np.fft.irfft(augmented)

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".WAV"
                tmp_series["Path"] = new_path
                n += 1

                df = df.append(tmp_series)

                # save augmented file
                soundfile.write(
                    file=new_path, data=augmented, samplerate=sr)

                j += 1

        d = {}  # dictionary for correct sequential numbering of augmented calls

        # augment random signals again to reach N
        while j < N:

            # random original call
            i = np.random.randint(0, metadata.shape[0] - 1)
            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sig = signals[i]  # signal
            sr = sample_rates[i]  # sampling rate of the signal

            # sequential numbering
            if not(call_file in d.keys()):
                n = how_many + 1
                d["call_file"] = [n]
            else:
                n = how_many + 1 + len(d["call_file"])
                d["call_file"].append(n)

            sig_freq = np.fft.rfft(sig)  # sig fft
            sig_freq_abs = np.abs(sig_freq)  # absolute sig fft

            formants = np.where((sig_freq_abs < np.max(sig_freq_abs))
                                * (sig_freq_abs > threshold * np.max(sig_freq_abs)))
            f0 = np.where(sig_freq_abs == np.max(
                sig_freq_abs))[0]  # fundamental

            # noise for substitution (from the same signal)
            where_noise = np.where(
                (sig_freq_abs < np.max(sig_freq_abs) * 0.05))

            shift_Hz = np.random.choice(
                [-1, 1]) * np.random.uniform(min_shift_Hz, max_shift_Hz)

            s = int(np.round(shift_Hz * len(sig) / sr))

            augmented = sig_freq.copy()

            for i in formants[0]:
                # exclude close to f0
                if i > (f0-30)[0] and i < (f0+31)[0]:
                    pass
                else:
                    start = (i - 15)
                    end = (i + 16)
                    if start < 0:
                        start = 0

                    if end > (len(sig_freq) - 1):
                        end = len(sig_freq) - 1

                    if (start + s) < 0:
                        s *= -1
                    elif (end + s) > (len(sig_freq) - 1):
                        s *= -1

                    start_s = start + s
                    end_s = end + s

                    noise = np.random.choice(sig_freq[where_noise], size=len(
                        sig_freq[start:end]))  # random noise
                    augmented[start:end] = np.random.choice(
                        noise, size=end-start)
                    augmented[start_s:end_s] = sig_freq[start:end]

            augmented = np.fft.irfft(augmented)

            # metadata rewrite
            tmp_series["CallFile"] = call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            new_path = directory + "\\" + call_file + \
                "_aug_{:02}".format(n) + ".WAV"
            tmp_series["Path"] = new_path
            n += 1

            df = df.append(tmp_series)

            # save augmented file
            soundfile.write(
                file=new_path, data=augmented, samplerate=sr)

            j += 1

        return df

    def HarmDist(self, signals, sample_rates, metadata, directory):

        df = pd.DataFrame(columns=metadata.columns)

        for i, sig in enumerate(signals):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]  # sampling rate of the signal

            augmented = 0.8 * np.sin(2 * np.pi * sig)

            # metadata rewrite
            tmp_series["CallFile"] = call_file + ".WAV"
            new_path = directory + "\\" + call_file + ".WAV"
            tmp_series["Path"] = new_path
            df = df.append(tmp_series)

            # save augmented file
            soundfile.write(
                file=new_path, data=augmented, samplerate=sr)

        return df


class augment_specs:

    def __init__(self) -> None:
        pass

    def SpecAugment(self, specs, metadata, sample_rates, directory, N, aug_type="both", max_mask=0.1, fmin=100, fmax=4000, seed=42):
        """
        Spectrogram augmentation using SpecAugment framework

        Args:
            specs [list]: list of spectrogram images
            metadata [pd.DataFrame]: metadata dataframe
            sample_rates [list]: sampling rates
            directory [str]: directory where to save augmented spectrogram images
            N [int]: how many files in total
            aug_type [str]: what type of SpecAugment to do; 
                            options are freq (freq. masking), time (time masking), both (default), random (random choice between the other options for each augmentation).
            max_mask [float]: Default=0.1; maximum width of mask expressed as ratio of image dimensions.
            fmin: [int] Default=100 (working with meerkats). Minimum frequency.
            fmax: [int] Default=4000 (working with meerkats). Maximum frequency.
            seed [int]: random seed for reproducibility

        Returns:
            df [pd.DataFrame]: augmented dataframe
        """

        if not(aug_type in ["freq", "time", "both", "random"]):
            raise ValueError(
                "aug_type value does not correspond to any of the available options")

        df = metadata.copy(deep=True)

        np.random.seed(seed)

        n_calls = metadata.shape[0]

        # how many augmented signals per original call
        how_many = (N-n_calls)//n_calls

        j = 0  # call counter

        n_cols = specs[0].shape[1]
        n_rows = specs[0].shape[0]

        if aug_type == "random":
            is_random = True
        else:
            is_random = False

        for i, S in enumerate(specs):

            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            sr = sample_rates[i]
            n = 1  # sequential numbering of augmented signals

            j += 1  # keep count also of original signals

            for k in range(how_many):

                if is_random:
                    aug_type = np.random.choice(["freq", "time", "both"])

                if aug_type == "time":
                    # randomly select where to start mask
                    start = np.random.randint(
                        0, n_cols - int(max_mask*n_cols)-1)
                    end = start + int(max_mask*n_cols)

                    # mask selection
                    augmented = S.copy()
                    augmented[:, start:end+1] = 0.0

                if aug_type == "freq":
                    # randomly select where to start mask
                    start = np.random.randint(
                        0, n_rows - int(max_mask*n_rows)-1)
                    end = start + int(max_mask*n_rows)

                    # mask selection
                    augmented = S.copy()
                    augmented[start:end+1, :, :] = 0.0

                if aug_type == "both":
                    # randomly select where to start mask
                    start = np.random.randint(
                        0, n_cols - int(max_mask*n_cols)-1)
                    end = start + int(max_mask*n_cols)

                    # mask selection
                    augmented = S.copy()
                    augmented[:, start:end+1, :] = 0

                    # randomly select where to start mask
                    start = np.random.randint(
                        0, n_rows - int(max_mask*n_rows)-1)
                    end = start + int(max_mask*n_rows)

                    augmented[start:end+1, :] = 0

                # metadata rewrite
                tmp_series["CallFile"] = call_file + \
                    "_aug_{:02}".format(n) + ".png"
                new_path = directory + "\\" + call_file + \
                    "_aug_{:02}".format(n) + ".png"
                tmp_series["Path"] = new_path
                n += 1

                # save image
                matplotlib.image.imsave(new_path, augmented, cmap="gray")

                j += 1

                df = df.append(tmp_series)

        d = {}  # dictionary for correct sequential numbering of augmented calls

        # augment random signals again to reach N
        while j < N:

            # random original call
            i = np.random.randint(0, metadata.shape[0] - 1)
            # extract original call file metadata
            call_file = metadata["CallFile"].iloc[i][:-4]
            # create a metadata series obj to manipulate
            tmp_series = metadata.copy(deep=True).iloc[i]
            S = specs[i]
            sr = sample_rates[i]

            # sequential numbering
            if not(call_file in d.keys()):
                n = how_many + 1
                d["call_file"] = [n]
            else:
                n = how_many + 1 + len(d["call_file"])
                d["call_file"].append(n)

            if is_random:
                aug_type = np.random.choice(["freq", "time", "both"])

            if aug_type == "time":
                # randomly select where to start mask
                start = np.random.randint(
                    0, n_cols - int(max_mask*n_cols)-1)
                end = start + int(max_mask*n_cols)

                # mask selection
                augmented = S.copy()
                augmented[:, start:end+1] = 0.0

            if aug_type == "freq":
                # randomly select where to start mask
                start = np.random.randint(
                    0, n_rows - int(max_mask*n_rows)-1)
                end = start + int(max_mask*n_rows)

                # mask selection
                augmented = S.copy()
                augmented[start:end+1, :] = 0.0

            if aug_type == "both":
                # randomly select where to start mask
                start = np.random.randint(
                    0, n_cols - int(max_mask*n_cols)-1)
                end = start + int(max_mask*n_cols)

                # mask selection
                augmented = S.copy()
                augmented[:, start:end+1] = 0

                # randomly select where to start mask
                start = np.random.randint(
                    0, n_rows - int(max_mask*n_rows)-1)
                end = start + int(max_mask*n_rows)

                augmented[start:end+1, :] = 0.0

            # metadata rewrite
            tmp_series["CallFile"] = call_file + \
                "_aug_{:02}".format(n) + ".png"
            new_path = directory + "\\" + call_file + \
                "_aug_{:02}".format(n) + ".png"
            tmp_series["Path"] = new_path
            n += 1

            # save image
            matplotlib.image.imsave(new_path, augmented, cmap="gray")

            j += 1

            df = df.append(tmp_series)

        return df
