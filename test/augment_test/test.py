# %%
import os
import feature_extraction
import augment
import pandas as pd
import numpy as np
from importlib import reload
import matplotlib.pyplot as plt
import matplotlib
import librosa
import librosa.display
import cv2

# %%
test_dir = os.getcwd()

# %%
df = pd.read_csv("testing.csv")
df.info()

# %%
df = df.iloc[1:3]
df

# %%
os.chdir("C:/Users/alessandro/Documents/UZH/Thesis/")
sig, sr = feature_extraction.audio(df["Path"].to_list())
os.chdir(test_dir)

# %%
os.chdir("C:/Users/alessandro/Documents/UZH/Thesis/")
augmenter = augment.augment_audio("audio/noise/")
os.chdir(test_dir)
aug_df = augmenter.SigAugment(
    signals=sig, sample_rates=sr, metadata=df, directory=test_dir,
    N=6, shift=True
)
aug_df

# %%
os.chdir("C:/Users/alessandro/Documents/UZH/Thesis/")
sig, sr = feature_extraction.audio(aug_df["Path"].to_list())

# %%
fig, ax = plt.subplots(nrows=3, sharex=True, sharey=True)
librosa.display.waveplot(sig[1], sr=sr[1], ax=ax[0])
librosa.display.waveplot(sig[4], sr=sr[4], ax=ax[1])
librosa.display.waveplot(sig[5], sr=sr[5], ax=ax[2])
plt.show()

# %%
os.chdir(test_dir)
df = pd.read_csv("testing.csv")
df = df.iloc[1:3].copy(deep=True)
df

# %%
os.chdir("C:/Users/alessandro/Documents/UZH/Thesis/")
sig, sr = feature_extraction.audio(df["Path"].to_list())
os.chdir(test_dir)

# %%
_, S = feature_extraction.features(
    signals=sig, sample_rates=sr
)

# %%
sp, new_df = feature_extraction.spec_img(
    specs_dB=S, metadata_df=df, sample_rates=sr, directory=test_dir
)

# %%
augmenter = augment.augment_specs()
aug_df = augmenter.SpecAugment(
    specs=S, metadata=new_df, directory=test_dir, N=6, sample_rates=sr
)

# %%
reload(matplotlib)
reload(matplotlib.pyplot)

# %%
imgs = [cv2.imread(fyle) for fyle in aug_df["Path"].to_list()]

plt.figure(figsize=(4, 2))
plt.imshow(imgs[1])
plt.show()
plt.figure(figsize=(4, 2))
plt.imshow(imgs[4])
plt.show()
plt.figure(figsize=(4, 2))
plt.imshow(imgs[5])
plt.show()

# %%
