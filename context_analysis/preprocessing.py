# %%
import os
from librosa.core.spectrum import reassigned_spectrogram
import pandas as pd
from pandas.core.indexes.datetimes import date_range
import numpy as np

# augmentation modules
import feature_extraction
import augment

# %%
# load metadata
meta = pd.read_csv(
    "C:/Users/alessandro/Documents/UZH/Thesis/code/metadata.csv",
    index_col=0)
meta.head()

# %%
# dates to datetime
meta["RecDate"] = pd.to_datetime(meta["RecDate"], format="%Y-%m-%d")
meta.info()

# %%
# only digging
contexts = ["DI"]
cont = meta.loc[meta["CallType"].isin(contexts)].copy(deep=True)
print("Size: ", cont.size)
cont.groupby(["Code", "CallType"]).size()

# %%
# check if all individuals are included
print("# Individuals: ", len(cont["Code"].unique().tolist()))
cont["Code"].unique().tolist()

# %%
# check amount of calls per individual
cont.groupby(["Code"]).size()

# %%
# filter for those that have at least 40 calls
inds = (cont.groupby(["Code"]).size(
).loc[cont.groupby(["Code"]).size() > 15]).index.to_list()
cont_train = cont.loc[cont["Code"].isin(inds)].copy(deep=True)
cont_test = cont_train.copy(deep=True)

# %%
# check the individuals match
cont_train["Code"].unique()

# %%
cont_train.shape

# %%
# check amount of recordings per date per individual
cont_train.groupby(["Code", "RecDate"]).size()

# %%
# dictionary of dates to remove from training to use for testing
dates_torem = {
    "VRRM182": pd.to_datetime(["2016-08-06"]),
    "VUKF016": pd.to_datetime(["2015-01-23", "2015-02-20"]),
    "VUKF018": pd.to_datetime(["2014-08-22", "2015-01-23"]),
    "VUKM017": pd.to_datetime(["2015-01-23"]),
    "VZUF014": pd.to_datetime(["2014-10-08", "2016-12-04"])
}

# remove from training and keep in testing
for code in dates_torem.keys():
    cont_train.drop(cont_train[
        (cont_train.Code == code) & cont_train.RecDate.isin(dates_torem[code])
    ].index, inplace=True)
    cont_test.drop(cont_test[
        ~cont_test.RecDate.isin(dates_torem[code]) & (cont_test.Code == code)
    ].index, inplace=True)


# %%
# save training and testing
try:
    os.mkdir("datasets/")
except FileExistsError:
    pass

cont_train.to_csv("datasets/cont_train.csv", index=False)
cont_test.to_csv("datasets/cont_test.csv", index=False)

# %%
training = pd.read_csv(
    "C:/Users/alessandro/Documents/UZH/Thesis/code/general_identification/datasets/spec_training_bal.csv", index_col=0)
training.head()

# %%
# dates to datetime
training["RecDate"] = pd.to_datetime(
    training["RecDate"], format="%d/%m/%Y")
training.info()

# %%
# remove individuals that are not in the context training
training.drop(training[~training.Code.isin(
    cont_train.Code.unique())].index, inplace=True)
print(training.shape)
training.groupby(["Code"]).size()

# %% select only SC context
training.drop(
    training.loc[training.CallType != "SC"].index,
    inplace=True
)
training.shape

# %%
# separate original calls from augmented
ori = training[~training.Path.str.contains("aug_")]
ori.groupby("Code").size()

# %%
# reduce the number of calls to min of context training
ori = ori.groupby("Code").sample(n=cont_train.groupby(
    "Code").size().min(), random_state=23)
ori.groupby("Code").size()

# %%
os.chdir("C:/Users/alessandro/Documents/UZH/Thesis/")

# reduce number of calls to minimum
m = cont_train.groupby("Code").size().min()
cont_train = cont_train.groupby("Code").sample(
    n=m, random_state=43)

# create the original spec imgs and augment
N = m * 115  # how many augmented files

tr = pd.DataFrame(columns=cont.columns)

# init augmenter
augmenter = augment.augment_specs()

for IND in cont_train["Code"].unique():
    print("INDIVIDUAL: ", IND)

    tmp_df = cont_train.loc[cont_train["Code"] == IND].copy(deep=True)
    print("before: ", tmp_df.shape)

    # load the original signals
    sig, sr = feature_extraction.audio(audio_files=tmp_df["Path"].to_list())

    # extract melspectrograms
    _, S = feature_extraction.features(
        signals=sig, sample_rates=sr
    )

    # make directory if it doesn't exist
    try:
        os.makedirs("spectrograms\\" + IND)
    except FileExistsError:
        pass

    # save the spectrogram images
    S, tmp_df = feature_extraction.spec_img(
        specs_dB=S, metadata_df=tmp_df, sample_rates=sr,
        directory="spectrograms\\" + IND, overwrite=True
    )

    # make directory if it doesn't exist
    try:
        os.makedirs("spectrograms\\" + IND +
                    "\\augmented\\context_analysis\\training")
    except FileExistsError:
        pass

    # augment spectrogram images
    tmp_df = augmenter.SpecAugment(
        specs=S, metadata=tmp_df,
        directory="spectrograms\\" + IND + "\\augmented\\context_analysis\\training",
        N=N, aug_type="random", max_mask=0.2, seed=56
    )

    print("after: ", tmp_df.shape)

    tr = tr.append(tmp_df, ignore_index=True)

print(tr.Path.to_list()[0:5])

# %% save augmented spec training (context)
tr.to_csv("code/context_analysis/datasets/spec_cont_train.csv", index=False)

# %%
# create a reference to original call file


def ref(call_files):

    call_ori = []

    for i in call_files:
        call = i.split("_")[:-2]
        call = "_".join(call) + ".WAV"

        call_ori.append(call)

    return call_ori


# %%
# create reference to original file
tmp = training[training.Path.str.contains("aug_")]
tmp.drop(tmp[~tmp.SourceFile.isin(ori.SourceFile)].index, inplace=True)
tmp["CallFileOri"] = ref(call_files=tmp.CallFile)
tmp.head()

# %% find augmented files for SC-FO only originals
tr_sc = ori.copy(deep=True)

for FILE in ori.CallFile:
    tmp_df = tmp.loc[tmp.CallFileOri == FILE].copy(deep=True)

    tmp_df = tmp_df.sample(n=114, random_state=56)

    tr_sc = tr_sc.append(tmp_df.iloc[:, 0:-1])

tr_sc.groupby("Code").size()

# %%
tr_sc.to_csv(
    "C:/Users/alessandro/Documents/UZH/Thesis/code/context_analysis/datasets/spec_training.csv", index=False)

# %%
# reduce numer of files for testing to minimum and create spectrogram images
m = cont_test.groupby("Code").size().min()
cont_test = cont_test.groupby("Code").sample(
    n=m, random_state=33)
cont_test.groupby("Code").size()

# create original spectrograms for testing
ts = pd.DataFrame(columns=cont_test.columns)

for IND in cont_test["Code"].unique():
    print("INDIVIDUAL: ", IND)

    tmp_df = cont_test.loc[cont_test["Code"] == IND].copy(deep=True)

    # load the original signals
    sig, sr = feature_extraction.audio(audio_files=tmp_df["Path"].to_list())

    # extract melspectrograms
    _, S = feature_extraction.features(
        signals=sig, sample_rates=sr
    )

    # make directory if it doesn't exist
    try:
        os.makedirs("spectrograms\\" + IND)
    except FileExistsError:
        pass

    # save the spectrogram images
    _, tmp_df = feature_extraction.spec_img(
        specs_dB=S, metadata_df=tmp_df, sample_rates=sr,
        directory="spectrograms\\" + IND, overwrite=True
    )

    ts = ts.append(tmp_df)

print(ts.Path.to_list()[0:5])

# %% save testing
ts.to_csv("code/context_analysis/datasets/spec_cont_test.csv", index=False)

# %% original testing
testing = pd.read_csv(
    "code/general_identification/datasets/testing_specs.csv", index_col=0)
testing.head()

# %%
# dates to datetime
testing["RecDate"] = pd.to_datetime(
    testing["RecDate"], format="%d/%m/%Y")
testing.info()

# %%
# remove individuals that are not in the context training
testing.drop(testing[~testing.Code.isin(
    cont_train.Code.unique())].index, inplace=True)
testing.shape

# %% select only SC
testing.drop(
    testing.loc[testing.CallType != "SC"].index,
    inplace=True
)
testing.shape

# %% reduce to minimm of testing context
m = cont_test.groupby("Code").size().min()
testing = testing.groupby("Code").sample(n=m, random_state=67)
testing.groupby("Code").size()

# %% save testing df
testing.to_csv("code/context_analysis/datasets/spec_testing.csv", index=False)

# %%
# create a mixed context testing
mixed = ts.append(testing)
mixed.shape

# %%
mixed.groupby("Code").size()

# %%
new_mixed = pd.DataFrame(columns=mixed.columns)

np.random.seed(73)
for IND in mixed.Code.unique():

    tmp = mixed.loc[mixed.Code == IND].copy(deep=True)

    if m % 2 != 0:
        n = np.random.randint(m//2, m//2 + 2)  # random integer for sample
    else:
        n = m//2

    tmp_df_1 = tmp.loc[tmp.CallType.isin(["SC", "FO"])].sample(
        n=n, random_state=np.random.randint(0, 100)
    ).copy(deep=True)

    if n == m//2 and m % 2 != 0:
        tmp_df_2 = tmp.loc[tmp.CallType.isin(["EA", "DI"])].sample(
            n=n+1, random_state=np.random.randint(0, 100)
        ).copy(deep=True)

    elif m % 2 != 0:
        tmp_df_2 = tmp.loc[tmp.CallType.isin(["EA", "DI"])].sample(
            n=n-1, random_state=np.random.randint(0, 100)
        ).copy(deep=True)

    elif m % 2 == 0:
        tmp_df_2 = tmp.loc[tmp.CallType.isin(["EA", "DI"])].sample(
            n=n, random_state=np.random.randint(0, 100)
        ).copy(deep=True)

    new_mixed = new_mixed.append(tmp_df_1)
    new_mixed = new_mixed.append(tmp_df_2)

new_mixed.groupby(["Code", "CallType"]).size()

# %% save mixed datasest
new_mixed.to_csv("code/context_analysis/datasets/mixed_test.csv", index=False)

# %%
os.chdir("C:/Users/alessandro/Documents/UZH/Thesis/code/context_analysis")

# load to make mixed context training
ctrain = pd.read_csv("datasets/spec_cont_train.csv")
train = pd.read_csv("datasets/spec_training.csv")

# %%
# original files only
# separate original calls from augmented
cori = ctrain[~ctrain.Path.str.contains("aug_")]
ori = train[~train.Path.str.contains("aug_")]

# %%
m = cori.groupby("Code").size().min()  # number of calls

# sample m//2 original calls
cori = cori.groupby("Code").sample(n=m//2, random_state=42)
ori = ori.groupby("Code").sample(n=m//2, random_state=42)
cori.groupby("Code").size()

# %%
# create reference to original file
tmp = ctrain[ctrain.Path.str.contains("aug_")]
tmp.drop(tmp[~tmp.SourceFile.isin(cori.SourceFile)].index, inplace=True)
tmp["CallFileOri"] = ref(call_files=tmp.CallFile)
tmp.head()

# %% find augmented files for SC-FO only originals
tr = cori.copy(deep=True)

for FILE in cori.CallFile:
    tmp_df = tmp.loc[tmp.CallFileOri == FILE].copy(deep=True)

    tmp_df = tmp_df.sample(n=114, random_state=56)

    tr = tr.append(tmp_df.iloc[:, 0:-1])

tr.groupby("Code").size()

# %%
# create reference to original file
tmp = train[train.Path.str.contains("aug_")]
tmp.drop(tmp[~tmp.SourceFile.isin(ori.SourceFile)].index, inplace=True)
tmp["CallFileOri"] = ref(call_files=tmp.CallFile)
tmp.head()

# %% find augmented files for SC-FO only originals
tr2 = ori.copy(deep=True)

for FILE in ori.CallFile:
    tmp_df = tmp.loc[tmp.CallFileOri == FILE].copy(deep=True)

    tmp_df = tmp_df.sample(n=114, random_state=56)

    tr2 = tr2.append(tmp_df.iloc[:, 0:-1])

tr2.groupby("Code").size()

# %%
mixed_training = tr.copy(deep=True)
mixed_training = mixed_training.append(tr2)

mixed_training.groupby("Code").size()

# %%
mixed_training.groupby("CallType").size()
# %%
mixed_training.to_csv("datasets/spec_mixed_train.csv", index=False)
# %%
