# %%
import os
from numpy.random import sample
import pandas as pd
from pandas.core.indexes.datetimes import date_range
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# augmentation modules
import feature_extraction
import augment

# %%
# load metadata
meta = pd.read_csv(
    "C:/Users/alessandro/Documents/UZH/Thesis/code/metadata.csv",
    index_col=0)
meta.head()

# %%
g = sns.FacetGrid(meta, col="Code", col_wrap=4)
g.map(sns.histplot, "RecDate")
g.set_xticklabels(rotation=30)

# %%
meta["Year"] = pd.DatetimeIndex(meta["RecDate"]).year
meta.groupby(["Code", "Year"]).size()

# %%
n_calls = pd.DataFrame(
    meta.groupby(["Code", "Year"]).size().to_frame("n").reset_index(),
)

sns.set_palette("bright")
plt.figure(figsize=(20, 20))
sns.barplot(
    data=n_calls, x="Year", y="n", hue="Code"
)

# %%
# age of the individuals in years (rounded)
meta["age"] = meta["AgeYears"].astype(int)
age = meta.groupby(["Code", "age"]).size().to_frame("n").reset_index()
age

# %%
g = sns.FacetGrid(meta, col="Code", col_wrap=4)
g.map(sns.histplot, "age")

# %%
# individuals to select
inds = [
    "VBBF088", "VBBF093", "VRRF159", "VUKF018", "VZUF014"
]
meta_sel = meta.loc[meta.Code.isin(inds)].copy(deep=True)
meta_sel.groupby("Code").size()

# %%
# check contexts distribution
meta_sel.groupby(["Code", "CallType"]).size()

# %%
# select only SC and FO for ven distribution
contexts = ["SC", "FO"]
meta_sel.drop(
    meta_sel.loc[~meta_sel.CallType.isin(contexts)].index,
    inplace=True
)
meta_sel.reset_index(drop=True, inplace=True)
meta_sel.groupby(["Code", "CallType"]).size()

# %%
# add column for age category division


def age_cat(df):

    cat = np.empty(shape=(df.shape[0]), dtype=object)

    idx = df.loc[df.AgeYears < 1].index
    cat[idx] = "juvenile/subadult"

    idx = df.loc[(df.AgeYears >= 1) & (df.AgeYears < 2)].index
    cat[idx] = "yearling"

    idx = df.loc[df.AgeYears >= 2].index
    cat[idx] = "adult"

    return cat


# %%
meta_sel["AgeCategory"] = age_cat(df=meta_sel)
meta_sel.AgeCategory.unique()

# %%
meta_sel.groupby(["Code", "AgeCategory"]).size()

# %%
# remove VRRF159: only 3 yearling calls
meta_sel.drop(
    meta_sel.loc[meta_sel.Code == "VRRF159"].index,
    inplace=True
)
meta_sel.reset_index(drop=True, inplace=True)
meta_sel.Code.unique()

# %%
os.chdir("C:/Users/alessandro/Documents/UZH/Thesis")

# create the spectrogram images
for IND in meta_sel.Code.unique():
    print(f"Individual:{IND}")

    sig, sr = feature_extraction.audio(audio_files=meta_sel.Path.to_list())
    _, S = feature_extraction.features(signals=sig, sample_rates=sr)

    try:
        os.makedirs("spectrograms\\" + IND)
    except FileExistsError:
        pass

    _, meta_sel = feature_extraction.spec_img(
        specs_dB=S, metadata_df=meta_sel, sample_rates=sr,
        directory=f"spectrograms\\\{IND}", overwrite=False
    )

print(meta_sel.Path.to_list()[0:5])

# %%
# training datasets
tr_j = meta_sel.loc[meta_sel.AgeCategory ==
                    "juvenile/subadult"].copy(deep=True)
tr_j.reset_index(drop=True, inplace=True)

tr_y = meta_sel.loc[meta_sel.AgeCategory == "yearling"].copy(deep=True)
tr_y.reset_index(drop=True, inplace=True)

tr_a = meta_sel.loc[meta_sel.AgeCategory == "adult"].copy(deep=True)
tr_a.reset_index(drop=True, inplace=True)

# %%
# find minimum for downsampling
m = meta_sel.groupby(["Code", "AgeCategory"]).size().min()

tr_j = tr_j.groupby("Code").sample(n=m, random_state=43)
tr_y = tr_y.groupby("Code").sample(n=m, random_state=43)
tr_a = tr_a.groupby("Code").sample(n=m, random_state=43)

# %%
# create testing datasets
ts_ya = tr_y.copy(deep=True)
ts_ya = ts_ya.append(tr_a)

ts_ja = tr_j.copy(deep=True)
ts_ja = ts_ja.append(tr_a)

ts_jy = tr_j.copy(deep=True)
ts_jy = ts_jy.append(tr_y)

# %%
# save testing datasets
try:
    os.makedirs(
        "C:/Users/alessandro/Documents/UZH/Thesis/code/stability_analysis/datasets")
except FileExistsError:
    pass

ts_ya.to_csv(
    "C:/Users/alessandro/Documents/UZH/Thesis/code/stability_analysis/datasets/testing_ya.csv", index=False)
ts_ja.to_csv(
    "C:/Users/alessandro/Documents/UZH/Thesis/code/stability_analysis/datasets/testing_ja.csv", index=False)
ts_jy.to_csv(
    "C:/Users/alessandro/Documents/UZH/Thesis/code/stability_analysis/datasets/testing_jy.csv", index=False)

# %%
# augmentation training data
N = m * 115

tr_j_new = pd.DataFrame(columns=meta_sel.columns)
tr_a_new = pd.DataFrame(columns=meta_sel.columns)
tr_y_new = pd.DataFrame(columns=meta_sel.columns)

augmenter = augment.augment_specs()

for IND in tr_j.Code.unique():
    print("Individual: ", IND)

    tmp_df = tr_j.loc[tr_j.Code == IND].copy(deep=True)
    print("before: ", tmp_df.shape)

    S, tmp_df = feature_extraction.spec_img(
        specs_dB=None, metadata_df=tmp_df, sample_rates=None,
        directory=None, created=True
    )

    try:
        os.makedirs("spectrograms\\" + IND +
                    "\\augmented\\stability_analysis\\juveniles_subadults")
    except FileExistsError:
        pass

    tmp_df = augmenter.SpecAugment(
        specs=S, metadata=tmp_df,
        directory="spectrograms\\" + IND +
        "\\augmented\\stability_analysis\\juveniles_subadults",
        N=N, aug_type="random", max_mask=0.2, seed=76
    )

    print("after: ", tmp_df.shape)
    tr_j_new = tr_j_new.append(tmp_df, ignore_index=True)

    tmp_df = tr_y.loc[tr_y.Code == IND].copy(deep=True)
    print("before: ", tmp_df.shape)

    S, tmp_df = feature_extraction.spec_img(
        specs_dB=None, metadata_df=tmp_df, sample_rates=None,
        directory=None, created=True
    )

    try:
        os.makedirs("spectrograms\\" + IND +
                    "\\augmented\\stability_analysis\\yearlings")
    except FileExistsError:
        pass

    tmp_df = augmenter.SpecAugment(
        specs=S, metadata=tmp_df,
        directory="spectrograms\\" + IND + "\\augmented\\stability_analysis\\yearlings",
        N=N, aug_type="random", max_mask=0.2, seed=76
    )

    print("after: ", tmp_df.shape)
    tr_y_new = tr_y_new.append(tmp_df, ignore_index=True)

    tmp_df = tr_a.loc[tr_a.Code == IND].copy(deep=True)
    print("before: ", tmp_df.shape)

    S, tmp_df = feature_extraction.spec_img(
        specs_dB=None, metadata_df=tmp_df, sample_rates=None,
        directory=None, created=True
    )

    try:
        os.makedirs("spectrograms\\" + IND +
                    "\\augmented\\stability_analysis\\adults")
    except FileExistsError:
        pass

    tmp_df = augmenter.SpecAugment(
        specs=S, metadata=tmp_df,
        directory="spectrograms\\" + IND + "\\augmented\\stability_analysis\\adults",
        N=N, aug_type="random", max_mask=0.2, seed=76
    )

    print("after: ", tmp_df.shape)
    tr_a_new = tr_a_new.append(tmp_df, ignore_index=True)

# %%
print(f"{tr_j_new.shape}, {tr_y_new.shape}, {tr_a_new.shape}")

# %%
# save training datasets
tr_j_new.to_csv("code/stability_analysis/datasets/training_j.csv", index=False)
tr_y_new.to_csv("code/stability_analysis/datasets/training_y.csv", index=False)
tr_a_new.to_csv("code/stability_analysis/datasets/training_a.csv", index=False)
# %%
